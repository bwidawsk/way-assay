# way-assay

way: Wayland

assay: "To examine a substance to find out how pure it is or what its properties (= qualities)"

__way-assay__ is a test suite for Wayland compositors which is meant to determine
spec conformance as well as some amount of
[functional](https://en.wikipedia.org/wiki/Functional_testing) correctness. It
is designed to be usable for any Wayland compositor that implements the
__way-assay__ test harness.

## Installation

### Dependencies

[Rust/Cargo](https://forge.rust-lang.org/infra/other-installation-methods.html)

| lib | arch | debian | fedora | alpine |
| --- | ---          | ---            | ---            | --- |
| libwayland-client.so  | wayland  | libwayland-dev  | wayland-devel  | wayland-dev |

## Usage

```bash
WSSY_LIB=/f/b/b/foowlcs.so cargo test --test wayassay
```

## Environment

| ENV VAR | Required | Description |
| ---     | ---      | ---         |
| _WSSY_LIB_ | y | Path to the Compositor's WLCS integration library |
| _WSSY_CFG_ | n |  Path to a Ron config file |

## WLCS Note
__way-assay__ is working on a test harness protocol (see #3). In the meantime,
it can be run by any compositor that implements the [WLCS
interfaces](https://github.com/canonical/wlcs/tree/main/include/wlcs). 

See [wlcs](https://github.com/canonical/wlcs) for more details

## Support

Use this gitlab issue tracker.

__bwidawsk__ on Matrix/IRC (libera or oftc)

## Roadmap

See [dev README](/README_dev.md) for information geared toward developers, and
roadmap.

## License

MIT
