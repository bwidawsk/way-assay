# Information for developers

# Roadmap

- \[x\] Implement test runner capabilities
- \[x\] Implement basic set of helpers for writing core tests
- \[x\] Implement basic XDG helpers
- \[ \] Develop suitable ABI
  - \[ \] Solidify suitable interface that can be used cross-compositors for
 test.
  - \[ \] Implement ABI for way-assay
  - \[ \] Implement ABI for Weston
- \[ \] Develop initial set of tests
  - \[ \] Core: At least one use of all requests/events (minus wl_shell)
  - \[ \] XDG shell: At least one test for each
    - \[ \] xdg_toplevel
    - \[ \] xdg_popup
    - \[ \] xdg_position
- \[ \] Move repository to [central location](https://gitlab.freedesktop.org/wayland/)
