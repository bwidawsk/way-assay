//! Abstraction over integration with the Wayland server

#[cfg(feature = "wlcs")]
use libloading::{Library, Symbol};

#[cfg(feature = "wlcs")]
use crate::harness::dlopen::DlError;

mod dlopen;
pub mod test_harness;
#[cfg(feature = "wlcs")]
pub mod wlcs;
