//! Test harness abstracts the low level controls over the Wayland display
//! server.
//!
//! An example of such functionality would be to establish a Wayland connection
//! to the server. In a general case, this may be as simple as a unix domain
//! socket connection.

use std::any::Any;

use wayland_backend::client::Backend;
use wayland_client::{protocol::wl_surface::WlSurface, Connection};

pub trait WssyPointer {
    fn move_absolute(&self, x: f64, y: f64);
    fn move_relative(&self, x: f64, y: f64);
    fn button_press(&self, button: u32);
    fn button_release(&self, button: u32);
}

/// A Harness is the interface for Wayland test suite integration
///
/// The test harness represents the API that any Wayland test interface must
/// implement in order to be supported by this test infrastructure.
pub trait Harness: std::fmt::Debug + Clone + 'static {
    /// Get the backend of a Wayland client.
    ///
    /// The [Wayland client
    /// backend](https://smithay.github.io/smithay/wayland_backend/sys/client/struct.Backend.html)
    /// interacts with the client side backend ie. libwayland. Generally a test
    /// harness would create a client Wayland socket and obtain the backend
    /// from that.
    fn connection(&self) -> anyhow::Result<Connection>;

    /// Create a new Window
    ///
    /// Tell the Wayland implementation to create a new Window. What a window
    /// means is specific to the implementation.
    ///
    /// TODO: Add x, y, size, etc.
    fn create_window(&self, backend: wayland_client::backend::Backend);

    fn position_toplevel_absolute(&self, client: &Backend, surface: &WlSurface, x: f64, y: f64);

    fn create_pointer(&self) -> Box<dyn WssyPointer>;

    /// Casts this `Harness` to `Any`
    fn as_any(&self) -> &dyn Any {
        self
    }
}

/// The HarnessManager is responsible for managing the lifetime of the Harness
pub trait HarnessManager: Clone + Send + Sync + 'static {
    fn create_harness(&self) -> anyhow::Result<impl Harness + Send + Sync + 'static>;

    /// Stop and drop server
    fn drop_harness(&self, harness: impl Harness) -> anyhow::Result<()>;
}

#[derive(Clone, Debug)]
pub struct NullHarness;

impl Harness for NullHarness {
    fn connection(&self) -> anyhow::Result<Connection> {
        todo!()
    }

    fn create_window(&self, _backend: wayland_client::backend::Backend) {
        todo!()
    }

    fn position_toplevel_absolute(
        &self,
        _client: &Backend,
        _surface: &WlSurface,
        _x: f64,
        _y: f64,
    ) {
        todo!()
    }

    fn create_pointer(&self) -> Box<dyn WssyPointer> {
        todo!()
    }
}

#[derive(Clone)]
pub struct NullHarnessManager;

impl HarnessManager for NullHarnessManager {
    fn create_harness(&self) -> anyhow::Result<impl Harness> {
        Ok(NullHarness)
    }

    fn drop_harness(&self, _harness: impl Harness) -> anyhow::Result<()> {
        Ok(())
    }
}
