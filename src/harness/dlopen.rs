//! Open an external library. This is kindly donated from the
//! [dlib](https://github.com/elinorbgr/dlib) project.

/// An error generated when failing to load a library
#[derive(Debug)]
pub enum DlError {
    /// The requested library would not be opened
    ///
    /// Includes the error reported by `libloading` when trying to
    /// open the library.
    CantOpen(libloading::Error),
    /// Some required symbol was missing in the library
    MissingSymbol(&'static str),
}

impl std::error::Error for DlError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            DlError::CantOpen(ref e) => Some(e),
            DlError::MissingSymbol(_) => None,
        }
    }
}
impl std::fmt::Display for DlError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            DlError::CantOpen(ref e) => write!(f, "Could not open the requested library: {}", e),
            DlError::MissingSymbol(s) => write!(f, "The requested symbol was missing: {}", s),
        }
    }
}

#[doc(hidden)]
#[macro_export]
macro_rules! dlopen_external_library(
    (__struct, $structname: ident,
        $(statics: $($sname: ident: $stype: ty),+,)|*
        $(functions: $(fn $fname: ident($($farg: ty),*) -> $fret:ty),+,)|*
        $(varargs: $(fn $vname: ident($($vargs: ty),+) -> $vret: ty),+,)|*
    ) => (
        struct $structname {
            __lib: $crate::harness::Library,
            $($(
                $sname: $crate::harness::Symbol<'static, &'static $stype>,
            )+)*
            $($(
                $fname: $crate::harness::Symbol<'static, unsafe extern "C" fn($($farg),*) -> $fret>,
            )+)*
            $($(
                $vname: $crate::harness::Symbol<'static, unsafe extern "C" fn($($vargs),+ , ...) -> $vret>,
            )+)*
        }
    );
    (__impl, $structname: ident,
        $(statics: $($sname: ident: $stype: ty),+,)|*
        $(functions: $(fn $fname: ident($($farg: ty),*) -> $fret:ty),+,)|*
        $(varargs: $(fn $vname: ident($($vargs: ty),+) -> $vret: ty),+,)|*
    ) => (
    impl $structname {
        /// # Safety
        unsafe fn open(name: &str) -> Result<$structname, $crate::harness::DlError> {
            // we use it to ensure the 'static lifetime
            use std::mem::transmute;
            let lib = $crate::harness::Library::new(name).map_err($crate::harness::DlError::CantOpen)?;
            let s = $structname {
                $($($sname: {
                    let s_name = concat!(stringify!($sname), "\0");
                    transmute(match lib.get::<&'static $stype>(s_name.as_bytes()) {
                        Ok(s) => s,
                        Err(_) => return Err($crate::harness::DlError::MissingSymbol(s_name))
                    })
                },
                )+)*
                $($($fname: {
                    let s_name = concat!(stringify!($fname), "\0");
                    transmute(match lib.get::<unsafe extern "C" fn($($farg),*) -> $fret>(s_name.as_bytes()) {
                        Ok(s) => s,
                        Err(_) => return Err($crate::harness::DlError::MissingSymbol(s_name))
                    })
                },
                )+)*
                $($($vname: {
                    let s_name = concat!(stringify!($vname), "\0");
                    transmute(match lib.get::<unsafe extern "C" fn($($vargs),+ , ...) -> $vret>(s_name.as_bytes()) {
                        Ok(s) => s,
                        Err(_) => return Err($crate::harness::DlError::MissingSymbol(s_name))
                    })
                },
                )+)*
                __lib: lib
            };
            Ok(s)
        }
    }
    );
    ($structname: ident,
        $(statics: $($sname: ident: $stype: ty),+,)|*
        $(functions: $(fn $fname: ident($($farg: ty),*) -> $fret:ty),+,)|*
        $(varargs: $(fn $vname: ident($($vargs: ty),+) -> $vret: ty),+,)|*
    ) => (
        $crate::dlopen_external_library!(__struct,
            $structname, $(statics: $($sname: $stype),+,)|*
            $(functions: $(fn $fname($($farg),*) -> $fret),+,)|*
            $(varargs: $(fn $vname($($vargs),+) -> $vret),+,)|*
        );
        $crate::dlopen_external_library!(__impl,
            $structname, $(statics: $($sname: $stype),+,)|*
            $(functions: $(fn $fname($($farg),*) -> $fret),+,)|*
            $(varargs: $(fn $vname($($vargs),+) -> $vret),+,)|*
        );
        unsafe impl Sync for $structname { }
    );
);
