//! Integration with WLCS ABI

use std::{
    ffi::{c_char, CStr, CString},
    fmt::Debug,
    os, slice,
    sync::{Arc, Mutex},
};

use anyhow::Context;

use tracing::{info, warn};
use wayland_backend::client::Backend;
use wayland_client::{protocol::wl_surface::WlSurface, Connection, Proxy};
use wayland_sys::common::wl_fixed_from_double;
use wlcs::{ffi_display_server_api::WlcsDisplayServer, ffi_pointer_api::WlcsPointer};

use crate::{dlopen_external_library, test_common::EarlyParameters};

use super::test_harness::{Harness, HarnessManager, WssyPointer};

dlopen_external_library!(Wlcs,
statics:
    wlcs_server_integration: wlcs::ffi_display_server_api::WlcsServerIntegration,
);

macro_rules! wlcs_call {
    ($server_ptr:ident, $func_name:ident, $($arg: expr),*) => {
        unsafe {
            (*$server_ptr).$func_name.unwrap()($server_ptr, $($arg),*)
        }
    };
    ($server_ptr:ident, $func_name:ident) => {
        unsafe {
            (*$server_ptr).$func_name.unwrap()($server_ptr)
        }
    };
}

#[derive(Debug)]
struct InnerWlcsConnection {
    library: Arc<Wlcs>,
    server_ptr: *mut WlcsDisplayServer,
    extensions: Vec<(String, usize)>,
    c_args: [*const i8; 2],
}

impl Debug for Wlcs {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Wlcs")
            .field("__lib", &self.__lib)
            .field("wlcs_server_integration", &self.wlcs_server_integration)
            .finish()
    }
}

#[derive(Debug, Clone)]
/// Handle to a WLCS connection
pub struct WlcsConnection {
    inner: Arc<Mutex<InnerWlcsConnection>>,
}

unsafe impl Send for InnerWlcsConnection {}
unsafe impl Sync for InnerWlcsConnection {}

impl Drop for InnerWlcsConnection {
    fn drop(&mut self) {
        unsafe {
            if let Some(destroy_fn) = self.library.wlcs_server_integration.destroy_server {
                destroy_fn(self.server_ptr);
            }
        }

        // Bring the program name back into rust space and drop it
        if !self.c_args[0].is_null() {
            let _ = unsafe { CString::from_raw(self.c_args[0] as *mut c_char) };
        }
    }
}

impl WlcsConnection {
    pub fn new(path: &str) -> anyhow::Result<Self> {
        let wlcs = unsafe { Wlcs::open(path)? };
        let integration = wlcs.wlcs_server_integration.clone();
        if integration.version != 3 {
            warn!("Unknown WLCS version {}", integration.version);
        }
        info!("Connected to WLCS server version {}", integration.version);

        let (program_name, c_len) = std::env::args()
            .next()
            .map_or(Default::default(), |s| (CString::new(s).unwrap(), 1));

        // Mir segfaults if argv is null and fails if it doesn't recognise arguments so
        // we'll do this just give it the first argument for now.
        let mut c_args = [core::ptr::null() as *const c_char; 2];
        c_args[0] = program_name.into_raw();

        let server_ptr = unsafe {
            wlcs.wlcs_server_integration.create_server.unwrap()(c_len, c_args.as_mut_ptr())
        };

        let descriptor = wlcs_call!(server_ptr, get_descriptor);
        let descriptors = unsafe {
            let descriptor = *descriptor;
            slice::from_raw_parts(descriptor.supported_extensions, descriptor.num_extensions)
        };

        let extensions = descriptors
            .iter()
            .map(|d| {
                let cstr = unsafe { CStr::from_ptr(d.name) };
                let name = String::from_utf8_lossy(cstr.to_bytes()).to_string();
                (name, d.version as usize)
            })
            .collect();

        let inner = InnerWlcsConnection {
            library: Arc::new(wlcs),
            server_ptr,
            extensions,
            c_args,
        };
        Ok(Self {
            inner: Arc::new(Mutex::new(inner)),
        })
    }

    /// Start the display server's mainloop.
    /// This should *not* block until the mainloop exits, which implies the
    /// mainloop will need to be run in a separate thread.
    ///
    /// This does not need to block until the display server is ready to process
    /// input, but the WlcsDisplayServer does need to be able to process
    /// other calls (notably create_client_socket) once this returns.
    pub fn start(&self) {
        let sp = self.inner.lock().unwrap().server_ptr;
        wlcs_call!(sp, start)
    }

    /// Stop the display server's mainloop.
    /// In contrast to the start hook, this *should* block until the server's
    /// mainloop has been torn down, so that it does not persist into later
    /// tests.
    pub fn stop(&self) {
        let sp = self.inner.lock().unwrap().server_ptr;
        wlcs_call!(sp, stop)
    }

    /// Create a socket that can be connected to by wl_display_connect_fd return
    /// A FD to a client Wayland socket. WLCS owns this fd, and will close
    /// it as necessary.
    pub fn create_client_socket(&self) -> os::fd::OwnedFd {
        let sp = self.inner.lock().unwrap().server_ptr;
        let fd: os::fd::RawFd = wlcs_call!(sp, create_client_socket);
        unsafe { <os::fd::OwnedFd as os::fd::FromRawFd>::from_raw_fd(fd) }
    }

    /// Position a window in the compositor coordinate space
    /// client   the (wayland-client-side) wl_client which owns the surface
    /// surface  the (wayland-client-side) wl_surface
    /// x         x coordinate (in compositor-space pixels) to move the left of
    /// the window to y         y coordinate (in compositor-space pixels) to
    /// move the top of the window to
    ///
    /// pub position_window_absolute: ::std::option::Option<
    ///    unsafe extern "C" fn(
    ///        server: *mut WlcsDisplayServer,
    ///        client: *mut wl_display,
    ///        surface: *mut wl_proxy,
    ///        x: ::std::os::raw::c_int,
    ///        y: ::std::os::raw::c_int,
    ///    ),
    /// >,
    #[allow(clippy::not_unsafe_ptr_arg_deref)]
    pub fn position_window_absolute(
        &self,
        _server: *mut WlcsDisplayServer,
        client: *mut wayland_sys::client::wl_display,
        surface: *mut wayland_sys::client::wl_proxy,
        x: isize,
        y: isize,
    ) {
        let sp = self.inner.lock().unwrap().server_ptr;
        wlcs_call!(
            sp,
            position_window_absolute,
            client,
            surface,
            x as i32,
            y as i32
        );
    }

    /// Create a fake pointer device
    /// pub create_pointer: ::std::option::Option<
    /// unsafe extern "C" fn(server: *mut WlcsDisplayServer) -> *mut
    /// WlcsPointer, >,
    pub fn create_wlcs_pointer(&self) -> *mut WlcsPointer {
        let sp = self.inner.lock().unwrap().server_ptr;
        wlcs_call!(sp, create_pointer)
    }
}

impl Harness for WlcsConnection {
    fn connection(&self) -> anyhow::Result<wayland_client::Connection> {
        Connection::from_socket(self.create_client_socket().into())
            .context("Failed to connect from socket")
    }

    fn position_toplevel_absolute(&self, display: &Backend, surface: &WlSurface, x: f64, y: f64) {
        self.position_window_absolute(
            std::ptr::null_mut(),
            display.display_ptr(),
            surface.id().as_ptr(),
            x as isize,
            y as isize,
        );
    }

    fn create_window(&self, _backend: wayland_client::backend::Backend) {
        todo!();
    }

    fn create_pointer(&self) -> Box<dyn super::test_harness::WssyPointer> {
        Box::new(WlcsWssyPointer(self.create_wlcs_pointer()))
    }
}

#[derive(Clone)]
pub struct WlcsHarnessManager {
    params: EarlyParameters,
}

impl WlcsHarnessManager {
    pub fn new(params: &EarlyParameters) -> Self {
        Self {
            params: params.clone(),
        }
    }
}

impl HarnessManager for WlcsHarnessManager {
    fn create_harness(&self) -> anyhow::Result<impl Harness> {
        let wlcs = WlcsConnection::new(&self.params.suite_path().to_string_lossy())?;
        wlcs.start();
        Ok(wlcs)
    }

    fn drop_harness(&self, harness: impl Harness) -> anyhow::Result<()> {
        harness
            .as_any()
            .downcast_ref::<WlcsConnection>()
            .context("Failed to downcast WlcsConnection")?
            .stop();
        Ok(())
    }
}

struct WlcsWssyPointer(*mut WlcsPointer);

impl Drop for WlcsWssyPointer {
    fn drop(&mut self) {
        unsafe { (*self.0).destroy.unwrap()(self.0) }
    }
}

impl WssyPointer for WlcsWssyPointer {
    fn move_absolute(&self, x: f64, y: f64) {
        unsafe {
            (*self.0).move_absolute.unwrap()(
                self.0,
                wl_fixed_from_double(x),
                wl_fixed_from_double(y),
            )
        }
    }

    fn move_relative(&self, dx: f64, dy: f64) {
        unsafe {
            (*self.0).move_relative.unwrap()(
                self.0,
                wl_fixed_from_double(dx),
                wl_fixed_from_double(dy),
            )
        }
    }

    fn button_press(&self, button: u32) {
        unsafe { (*self.0).button_down.unwrap()(self.0, button as i32) }
    }

    fn button_release(&self, button: u32) {
        unsafe { (*self.0).button_up.unwrap()(self.0, button as i32) }
    }
}
