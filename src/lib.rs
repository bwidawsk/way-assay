//! Test runner, helper library and collection of tests for Wayland protocol.
//!
//! way-assay aims to provide all that's need to write Wayland protocol and
//! functionality tests from the lowest to medium level of functionality. Some
//! topics that way-assay provides testing for are, negative testing (protocol
//! error generation), protocol conformance, and basic functionality testing.
//!
//! # Negative testing
//!
//! Negative testing asserts that requests which should result in protocol
//! errors do result in protocol errors. An example of such might be attaching
//! an invalidly sized buffer to a surface and making sure the `invalid_size`
//! error is returned.
//!
//! # Conformance
//!
//! Conformance testing validates that a sequence of requests yields a proper
//! sequence of events. Such an example of validating conformance would be
//! asserting that `shm_pool` creation generates a sufficient set of
//! corresponding `format` events.
//!
//! # Functionality
//!
//! Functionality validation can be thought of as a complex series of
//! conformance tests. An example of such a test might be to overlap two
//! surfaces and then validating that `wl_pointer::enter` events are generated
//! for the correct surface (that with the lowest Z-order).
//!
//! Common helpers are implemented in [`crate::test_common`].
//!
//! Functions that support hooking up tests to the Wayland server are located in
//! [`crate::harness`]
//!
//! Test cases are located in the tests/ directory.

pub mod harness;
pub mod test_common;
