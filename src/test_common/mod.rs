//! Set of consolidated helpers for creation of Wayland tests.
//!
//! The test framework is built around [wayland_client] but discards the notion
//! of [wayland_client::Dispatch] in favor of a more focused, and less verbose
//! dispatching mechanism ([WaylandTest::add_callback])
use std::{
    any::{Any, TypeId},
    cmp,
    collections::HashMap,
    default::Default,
    env,
    error::Error,
    fmt,
    io::ErrorKind,
    ops::{Deref, DerefMut},
    os::fd::{BorrowedFd, OwnedFd},
    path::PathBuf,
    sync::{Arc, Mutex, RwLock},
    time::{Duration, Instant},
};

use anyhow::Context;
use rustix::{
    event::{poll, PollFd, PollFlags},
    io::Errno,
};
use tracing::{error, info, instrument, trace, Level};
use wayland_backend::{
    client::{Backend, InvalidId, ObjectData, ObjectId, WaylandError},
    protocol::{self, Argument, Message, INLINE_ARGS},
    smallvec::SmallVec,
};
use wayland_client::{
    globals::{self, Global, GlobalList, GlobalListContents},
    protocol::{
        wl_callback::{self, WlCallback},
        wl_display::{self, WlDisplay},
        wl_registry::{self, WlRegistry},
    },
    Connection, Dispatch, DispatchError, EventQueue, Proxy, QueueHandle,
};

use crate::harness::test_harness::Harness;

use euclid::default::{Box2D, Point2D, Rect};

pub mod core;
pub mod stable;

/// Parameters for running running way-assay
///
/// These parameters should be environment variables.
#[derive(Clone)]
pub struct EarlyParameters {
    suite_path: PathBuf,
    cfg_path: Option<PathBuf>,
}

impl EarlyParameters {
    pub fn new() -> anyhow::Result<Self> {
        let lib = env::var("WSSY_LIB").context("Missing required WSSY_LIB env var")?;
        let cfg = env::var("WSSY_CFG");
        Ok(EarlyParameters {
            suite_path: lib.into(),
            cfg_path: cfg.ok().map(Into::into),
        })
    }

    /// Get the path to the integration harness, ie. foowlcs.so
    pub fn suite_path(&self) -> &PathBuf {
        &self.suite_path
    }

    pub fn cfg_path(&self) -> Option<&PathBuf> {
        self.cfg_path.as_ref()
    }
}

pub fn protocol_bind<I: Proxy, D: ObjectData>(
    registry: &WlRegistry,
    global: &Global,
    required_globals: &[WssyProtocol],
    data: Arc<D>,
) -> Option<(I, (&'static str, u32))> {
    let global_str = I::interface().name;

    // Bind the specific version set by the caller of the protocol, or fallback to
    // the latest version supported by the server.
    let version: u32 = required_globals
        .iter()
        .find(|r| r.0 .0 == global_str)
        .map_or(global.version, |w| w.0 .1);

    match registry.send_constructor::<I>(
        wl_registry::Request::Bind {
            name: global.name,
            id: (I::interface(), version),
        },
        data,
    ) {
        // Return the name as string, and version bound
        Ok(y) => Some((y, (global_str, version))),
        Err(e) => {
            info!("Couldn't bind {} {e}", global_str);
            None
        }
    }
}

pub trait RequestVersion {
    fn version(&'static self, version: u32) -> WssyProtocol;
}

impl RequestVersion for &str {
    fn version(&'static self, version: u32) -> WssyProtocol {
        WssyProtocol((self, version))
    }
}

#[derive(Debug, PartialEq)]
pub struct WssyProtocol((&'static str, u32));
#[derive(Debug, Default)]
pub struct WssyRequiredProtocols(Vec<WssyProtocol>);

impl WssyRequiredProtocols {
    /// Return true if the protocol version being tested is high enough. Filter
    /// out if so.
    fn extract_if_higher_protocol(&mut self, p: &WssyProtocol) -> bool {
        // No test for this specific protocol.
        if !self.iter().any(|proto| proto.0 .0 == p.0 .0) {
            return true;
        }

        // If the protocol is in the list, check that the versions match
        let l = self.0.len();
        self.0.retain(|g| g.0 .0 != p.0 .0 || p < g);
        assert!(l + 2 > self.0.len()); // More than one was removed
        l - 1 == self.0.len()
    }
}

impl fmt::Display for WssyRequiredProtocols {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.iter()
            .try_fold((), |_result, proto| writeln!(f, "{}", proto))
    }
}

impl From<Vec<WssyProtocol>> for WssyRequiredProtocols {
    fn from(value: Vec<WssyProtocol>) -> Self {
        Self(value)
    }
}

impl Deref for WssyRequiredProtocols {
    type Target = Vec<WssyProtocol>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for WssyRequiredProtocols {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0.as_mut()
    }
}

impl fmt::Display for WssyProtocol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} v{}", self.0 .0, self.0 .1)
    }
}

impl PartialOrd for WssyProtocol {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        if self.0 .0 != other.0 .0 {
            None
        } else {
            Some(self.0 .1.cmp(&other.0 .1))
        }
    }
}

impl From<protocol::Interface> for WssyProtocol {
    fn from(value: protocol::Interface) -> Self {
        Self((value.name, value.version))
    }
}

impl From<&protocol::Interface> for WssyProtocol {
    fn from(value: &protocol::Interface) -> Self {
        Self((value.name, value.version))
    }
}

impl From<(&'static str, u32)> for WssyProtocol {
    fn from(value: (&'static str, u32)) -> Self {
        Self((value.0, value.1))
    }
}

#[derive(Clone)]
enum WssyRegionRectKind {
    Add,
    Subtract,
}

#[derive(Clone)]
struct WssyRegionRect(WssyRegionRectKind, Box2D<i32>);

#[derive(Clone, Default)]
struct WssyRegion(Arc<Mutex<Vec<WssyRegionRect>>>);

impl WssyRegion {
    fn add(&mut self, rect: impl Into<Rect<i32>>) {
        self.0.lock().unwrap().push(WssyRegionRect(
            WssyRegionRectKind::Add,
            rect.into().to_box2d(),
        ));
    }

    fn subtract(&self, rect: impl Into<Rect<i32>>) {
        self.0.lock().unwrap().push(WssyRegionRect(
            WssyRegionRectKind::Subtract,
            rect.into().to_box2d(),
        ));
    }

    // TODO: Verify this code is accurate, it's adapted from anvil.
    pub fn contains(&self, point: impl Into<Point2D<i32>>) -> bool {
        let point = point.into();
        let mut contains = false;
        use WssyRegionRectKind::*;
        for WssyRegionRect(kind, rect) in self.0.lock().unwrap().iter() {
            if rect.contains(point) {
                match kind {
                    Add => contains = true,
                    Subtract => contains = false,
                }
            }
        }
        contains
    }
}

impl ObjectData for WssyRegion {
    fn event(
        self: Arc<Self>,
        _backend: &Backend,
        _msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        unreachable!()
    }

    fn destroyed(&self, _object_id: ObjectId) {}
}

#[derive(Default)]
pub struct EventlessState;

impl ObjectData for EventlessState {
    fn event(
        self: Arc<Self>,
        _backend: &Backend,
        _msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        None
    }

    fn destroyed(&self, object_id: ObjectId) {
        trace!("Destroyed {} (id: {object_id})", object_id.interface())
    }
}

#[derive(Debug)]
/// Data passed to callbacks and used for synchronization events
pub struct TestData;

/// Helper for running Wayland protocol tests
///
/// While individual tests may want to bind to specific globals, the helpers
/// rely on also having access to these. [`WaylandTest`] provides the ability to
/// register for callbacks from the events and also track enough state to be
/// useful.
///
/// This is the lowest level of test helper. If too much is automatigically
/// done, the internal queues may need to be used directly.
pub struct Client {
    pub connection: Connection,
    /// The display singleton.
    display: WlDisplay,

    q: EventQueue<TestData>,

    /// Data passed to individual tests and available from within the test.
    ///
    /// This structure is largely opaque to test writers. Generally the pass,
    /// fail, or wait functionality is all that is needed.
    test_data: Arc<RwLock<TestData>>,

    globals: GlobalList,

    states: HashMap<std::any::TypeId, Box<dyn Any>>,
}

impl Dispatch<wl_registry::WlRegistry, GlobalListContents> for Client {
    fn event(
        _state: &mut Self,
        _registry: &wl_registry::WlRegistry,
        _event: <wl_registry::WlRegistry as wayland_client::Proxy>::Event,
        _data: &GlobalListContents,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
    ) {
    }
}

pub trait ClientState: Any {
    /// Instantiate new client state
    fn new(
        client: &mut Client,
        protocols: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError>
    where
        Self: Sized;
}

pub fn wssy_poll(
    fd: BorrowedFd,
    timeout: Duration,
    flags: PollFlags,
) -> Result<PollFlags, WssyError> {
    let mut fds = [PollFd::new(&fd, flags)];
    let deadline = Instant::now() + timeout;
    loop {
        let timeout = deadline
            .saturating_duration_since(Instant::now())
            .as_millis()
            .try_into()
            .unwrap_or(i32::MAX);
        return match poll(&mut fds, timeout) {
            Err(Errno::INTR) => continue,
            Err(e) => Err(WssyError::from(e)),
            Ok(0) => Err(WssyError::Timeout),
            Ok(_) => Ok(fds[0].revents()),
        };
    }
}

impl Client {
    /// Create a new test.
    pub fn new<H: Harness>(harness: H) -> Self {
        let connection = harness.connection().unwrap();
        let wl_display = connection.display();
        let q = connection.new_event_queue();
        let (globals, _) = globals::registry_queue_init::<Client>(&connection).unwrap();
        Self {
            connection,
            display: wl_display,
            q,
            test_data: Arc::new(RwLock::new(TestData)),
            globals,
            states: HashMap::new(),
        }
    }

    pub fn get_registry<T: ObjectData>(&mut self, data: Arc<T>) -> WlRegistry {
        let registry = self
            .display
            .send_constructor(wl_display::Request::GetRegistry {}, data.clone())
            .unwrap();

        let _ = self.roundtrip();

        registry
    }

    pub fn with_state<S: ClientState>(
        mut self,
        protocols: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError> {
        let b = Box::new(S::new(&mut self, protocols)?);
        self.states.insert(TypeId::of::<S>(), b);
        Ok(self)
    }

    pub fn state<S: ClientState>(&self) -> &S {
        let anystate = self.states.get(&TypeId::of::<S>()).unwrap();
        anystate.as_ref().downcast_ref().unwrap()
    }

    pub fn state_mut<S: ClientState>(&mut self) -> &mut S {
        let anystate: &mut std::boxed::Box<dyn std::any::Any> =
            self.states.get_mut(&TypeId::of::<S>()).unwrap();
        anystate.as_mut().downcast_mut().unwrap()
    }

    /// Issue a synchronous roundtrip with the server.
    ///
    /// ```no_run
    /// use way_assay::{harness::test_harness::NullHarness, test_common::WaylandTestBuilder};
    /// let builder: WaylandTestBuilder<NullHarness> = todo!();
    /// let mut test = builder.create_test();
    /// test.roundtrip();
    /// ```
    #[instrument(skip(self), err(level = Level::TRACE))]
    pub fn roundtrip(&mut self) -> Result<usize, WssyError> {
        let mut td = self.test_data.try_write().unwrap();
        trace!("roundtrip");
        self.q.roundtrip(&mut td).map_err(WssyError::from)
    }

    pub fn sync(&mut self) -> WssyFuture<u32> {
        let future = WssyFuture::new();
        let sync_data = CallbackData {
            done: future.clone(),
        };

        let _callback: WlCallback = self
            .display
            .send_constructor(wl_display::Request::Sync {}, Arc::new(sync_data))
            .unwrap();

        future
    }

    pub fn process_events_until(
        &mut self,
        timeout: Duration,
        mut cond: impl FnMut() -> bool,
    ) -> Result<(), WssyError> {
        let mut td = self.test_data.try_write().unwrap();
        let finish = Instant::now() + timeout;
        while !cond() {
            if Instant::now() >= finish {
                return Err(WssyError::Timeout);
            }

            let Some(guard) = self.q.prepare_read() else {
                self.q.dispatch_pending(&mut td).map_err(WssyError::from)?;
                return Ok(());
            };
            let fd = guard.connection_fd();

            loop {
                match self.q.flush() {
                    Ok(_) => break,
                    // Continue with event processing to get potential
                    // protocol error causing the broken pipe.
                    Err(WaylandError::Io(e)) if e.kind() == ErrorKind::BrokenPipe => break,
                    Err(ref err @ WaylandError::Io(ref e)) if e.kind() != ErrorKind::WouldBlock => {
                        return Err(WssyError::from(err.clone()))
                    }
                    _ => wssy_poll(fd, finish - Instant::now(), PollFlags::OUT).map(|_| ())?,
                }
            }

            wssy_poll(fd, finish - Instant::now(), PollFlags::IN)?;

            match guard.read() {
                Err(WaylandError::Io(ref e)) if e.kind() == ErrorKind::WouldBlock => {}
                Err(e) => return Err(WssyError::WaylandError(e)),
                _ => {}
            }

            self.q.dispatch_pending(&mut td).map_err(WssyError::from)?;
        }
        Ok(())
    }
}

/// way-assay specific errors
///
/// The internal event queue will call into the [`ObjectData`] for the given
/// protocol. As part of this, an error might be generated.
#[derive(thiserror::Error, Debug)]
pub enum WssyError {
    #[error("Dispatch error ({0}) during roundtrip")]
    DispatchError(#[from] DispatchError),

    #[error("Invalid Id ({0}) for operation")]
    InvalidId(#[from] InvalidId),

    #[error("Invalid Interface (expected {expected:?}, found {found:?})")]
    InvalidIface { expected: String, found: String },

    #[error("Couldn't clone")]
    DupError(#[from] Box<dyn Error + Send + Sync>),

    #[error("Couldn't initialize requested state. Missing protocols: {0:?}")]
    StateError(WssyRequiredProtocols),

    #[error("Operation timed out")]
    Timeout,

    #[error("Wayland error {0}")]
    WaylandError(#[from] WaylandError),

    #[error("Rustix error {0}")]
    Rustix(#[from] Errno),
}

#[derive(Clone, Default)]
pub struct WssyFuture<T: Clone> {
    value: Arc<Mutex<Option<T>>>,
}

impl<T: Clone> WssyFuture<T> {
    pub fn new() -> Self {
        WssyFuture {
            value: Arc::new(Mutex::new(None)),
        }
    }

    pub fn set(&self, value: T) {
        *self.value.lock().unwrap() = Some(value);
    }

    pub fn get(&self) -> Option<T> {
        self.value.lock().unwrap().clone()
    }
}

/// Forwards messages to a list of ObjectDatas
/// Cannot be used for events that construct new objects
/// If the message contains an fd as an argument this will duplicate it
pub struct CloneMessage(Vec<Arc<dyn ObjectData>>);

impl CloneMessage {
    pub fn new(data_objects: Vec<Arc<dyn ObjectData>>) -> Arc<Self> {
        Arc::new(CloneMessage(data_objects))
    }
}

impl ObjectData for CloneMessage {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        for data in self.0.iter().cloned() {
            let msg = {
                let sender_id = msg.sender_id.clone();
                let opcode = msg.opcode;
                let args = msg
                    .args
                    .iter()
                    .map(|arg| match arg {
                        Argument::Int(val) => Argument::Int(*val),
                        Argument::Uint(val) => Argument::Uint(*val),
                        Argument::Fixed(val) => Argument::Fixed(*val),
                        Argument::Str(val) => Argument::Str(val.clone()),
                        Argument::Object(val) => Argument::Object(val.clone()),
                        Argument::NewId(val) => Argument::NewId(val.clone()),
                        Argument::Array(val) => Argument::Array(val.clone()),
                        Argument::Fd(val) => Argument::Fd(
                            val.try_clone()
                                .context("failed to clone argument fd")
                                .unwrap(),
                        ),
                    })
                    .collect::<SmallVec<[Argument<ObjectId, OwnedFd>; INLINE_ARGS]>>();

                Message {
                    sender_id,
                    opcode,
                    args,
                }
            };
            data.event(backend, msg);
        }
        None
    }

    fn destroyed(&self, _object_id: ObjectId) {}
}

/// Parses event using [Proxy::parse_event] and sends them to the callback
/// closure
pub struct EventHandler<P: Proxy>(Box<dyn Fn(P, <P as Proxy>::Event) + Send + Sync>);

impl<P> EventHandler<P>
where
    P: Proxy + Sync + Send + 'static,
{
    /// e.g. CallbackState::<WlPointer, _>::new(|event| { match event { .. } });
    pub fn new(cb: impl Fn(P, <P as Proxy>::Event) + Send + Sync + 'static) -> Arc<Self> {
        Arc::new(Self(Box::new(cb)))
    }
}

impl<P> ObjectData for EventHandler<P>
where
    P: Proxy + Sync + Send + 'static,
{
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((item, event)) = P::parse_event(&conn, msg) else {
            error!("Cannot parse message from {}", P::interface());
            return None;
        };

        self.0(item, event);

        None
    }

    fn destroyed(&self, _object_id: ObjectId) {}
}

pub struct CallbackData {
    pub done: WssyFuture<u32>,
}

impl ObjectData for CallbackData {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((_, event)) = WlCallback::parse_event(&conn, msg) else {
            error!("Failed to parse wl_callback event");
            return None;
        };

        match event {
            wl_callback::Event::Done { callback_data } => {
                trace!("callback done {}", callback_data);
                self.done.set(callback_data);
            }
            _ => unreachable!(),
        };

        None
    }

    fn destroyed(&self, _: ObjectId) {}
}
