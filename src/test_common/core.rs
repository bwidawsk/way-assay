//! Common state management for Wayland core protocol

/// Default pool size for SHM pool.
const SHM_POOL_SIZE: i32 = 32_000_000;

use std::{
    collections::{HashMap, HashSet},
    os::fd::{AsRawFd, OwnedFd},
    sync::{Arc, Mutex, RwLock},
};

use tracing::{error, instrument, trace};
use wayland_backend::{
    client::{Backend, InvalidId, ObjectData, ObjectId},
    protocol::Message,
};
use wayland_client::{
    protocol::{
        wl_buffer::{self, WlBuffer},
        wl_compositor::{self, WlCompositor},
        wl_output::{self, WlOutput},
        wl_pointer::WlPointer,
        wl_region::WlRegion,
        wl_seat::{self, Capability, WlSeat},
        wl_shm::{self, WlShm},
        wl_shm_pool::{self, WlShmPool},
        wl_subcompositor::{self, WlSubcompositor},
        wl_subsurface::WlSubsurface,
        wl_surface::WlSurface,
        wl_touch::WlTouch,
    },
    Connection, Proxy, WEnum,
};

use super::{
    protocol_bind, Client, ClientState, EventHandler, EventlessState, WssyError, WssyRegion,
    WssyRequiredProtocols,
};

#[derive(Debug, Default)]
pub struct ShmState {
    pub formats: Mutex<HashSet<wl_shm::Format>>,
}

impl ObjectData for ShmState {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((_, event)) = WlShm::parse_event(&conn, msg) else {
            error!("Bad event format");
            return None;
        };

        match event {
            wl_shm::Event::Format { format } => self
                .formats
                .lock()
                .unwrap()
                .insert(format.into_result().unwrap()),
            _ => unreachable!(),
        };

        None
    }

    fn destroyed(&self, _object_id: ObjectId) {}
}

pub struct SeatState {
    capabilities: Mutex<WEnum<Capability>>,
    name: Mutex<String>,
}

impl SeatState {
    fn new() -> Self {
        SeatState {
            capabilities: Mutex::new(WEnum::Value(Capability::empty())),
            name: Default::default(),
        }
    }
}

impl ObjectData for SeatState {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((_, event)) = WlSeat::parse_event(&conn, msg) else {
            error!("Bad seat");
            return None;
        };

        use wl_seat::Event;
        match event {
            Event::Capabilities { capabilities } => {
                *self.capabilities.lock().unwrap() = capabilities;
            }
            Event::Name { name } => {
                *self.name.lock().unwrap() = name;
            }
            _ => unreachable!(),
        };
        None
    }

    fn destroyed(&self, _object_id: ObjectId) {
        todo!()
    }
}

#[derive(Debug, Clone)]
pub struct OutputInfo {
    // geometry
    location: (i32, i32),
    physical_size: (i32, i32),
    subpixel: WEnum<wl_output::Subpixel>,
    make: String,
    model: String,
    transform: WEnum<wl_output::Transform>,
    // modes (flags, width, height, refresh)
    modes: Vec<(WEnum<wl_output::Mode>, i32, i32, i32)>,
    // scale
    scale: i32,
    // name
    name: Option<String>,
    // description
    description: Option<String>,
}

impl std::default::Default for OutputInfo {
    fn default() -> Self {
        OutputInfo {
            location: (0, 0),
            physical_size: (0, 0),
            subpixel: WEnum::Unknown(0),
            make: "".into(),
            model: "".into(),
            transform: WEnum::Unknown(0),
            modes: vec![],
            scale: 0,
            name: None,
            description: None,
        }
    }
}

#[derive(Default, Debug)]
pub struct OutputState {
    inner_current: RwLock<Option<OutputInfo>>,
    inner_pending: RwLock<OutputInfo>,
}

impl ObjectData for OutputState {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((_, event)) = WlOutput::parse_event(&conn, msg) else {
            error!("Bad seat");
            return None;
        };

        let mut output = self.inner_pending.write().unwrap();

        use wl_output::Event;
        match event {
            Event::Geometry {
                x,
                y,
                physical_width,
                physical_height,
                subpixel,
                make,
                model,
                transform,
            } => {
                output.location = (x, y);
                output.physical_size = (physical_width, physical_height);
                output.subpixel = subpixel;
                output.make = make;
                output.model = model;
                output.transform = transform;
            }
            Event::Mode {
                flags,
                width,
                height,
                refresh,
            } => {
                output.modes.push((flags, width, height, refresh));
            }
            Event::Done => {
                self.inner_current.write().unwrap().replace(output.clone());
            }
            Event::Scale { factor } => output.scale = factor,
            Event::Name { name } => output.name = Some(name),
            Event::Description { description } => output.description = Some(description),
            _ => {}
        };

        None
    }

    fn destroyed(&self, _object_id: ObjectId) {}
}

#[derive(Default, Clone)]
pub struct BufferBuilder {
    width: i32,
    height: i32,
    pool: Option<(WlShmPool, i32)>,
    object_data: Option<Arc<dyn ObjectData>>,
    format: Option<(i32, WEnum<wl_shm::Format>)>,
}

impl BufferBuilder {
    pub fn new(width: i32, height: i32) -> Self {
        Self {
            width,
            height,
            ..Default::default()
        }
    }

    /// Set the WlShmPool and offset to create the WlBuffer for
    pub fn with_pool(mut self, pool: &WlShmPool, offset: i32) -> Self {
        self.pool.replace((pool.clone(), offset));
        self
    }

    /// Set the ObjectData for the WlBuffer
    pub fn with_object_data(mut self, object_data: Arc<dyn ObjectData>) -> Self {
        self.object_data.replace(object_data);
        self
    }

    /// Set ObjectData for WlBuffer to run provided closure on
    /// wl_buffer::release
    pub fn with_release_handler<F: Fn(WlBuffer) + Send + Sync + 'static>(mut self, cb: F) -> Self {
        self.object_data.replace(EventHandler::new(
            move |buffer: WlBuffer, event| match event {
                wl_buffer::Event::Release => cb(buffer),
                _ => unreachable!(),
            },
        ));
        self
    }

    /// Set the format and stride for the WlBuffer
    pub fn with_format(mut self, stride: i32, format: wl_shm::Format) -> Self {
        self.format
            .replace((stride, wayland_client::WEnum::Value(format)));
        self
    }

    /// Build the WlBuffer
    /// This will be ARGB8888 with a new wl_shm_pool with a 0 offset unless
    /// otherwise specified.
    pub fn build(self, core: &CoreState) -> anyhow::Result<WlBuffer> {
        let (stride, format) = self.format.unwrap_or_else(|| {
            (
                self.width * 4,
                wayland_client::WEnum::Value(wl_shm::Format::Argb8888),
            )
        });

        let (pool, offset) = self
            .pool
            .or_else(|| {
                core.shm_create_pool(self.height * stride)
                    .ok()
                    .map(|a| Some((a, 0)))
                    .unwrap()
            })
            .unwrap();

        let object_data = self.object_data.unwrap_or_else(|| Arc::new(EventlessState));

        let buffer = pool.send_constructor(
            wl_shm_pool::Request::CreateBuffer {
                offset,
                width: self.width,
                height: self.height,
                stride,
                format,
            },
            object_data,
        )?;

        Ok(buffer)
    }
}

pub struct CoreState {
    pub compositor: Option<WlCompositor>,
    pub subcompositor: Option<WlSubcompositor>,
    pub shm: Option<WlShm>,
    pub shm_state: Arc<ShmState>,
    pub seat: Option<WlSeat>,
    pub seat_state: Arc<SeatState>,
    pub outputs: HashMap<WlOutput, Arc<OutputState>>,
}

impl ClientState for CoreState {
    fn new(
        client: &mut Client,
        protocols: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError> {
        let registry = client.globals.registry();
        let contents = client.globals.contents();
        let shm_state = Arc::new(ShmState {
            formats: Mutex::new(HashSet::new()),
        });
        let seat_state = Arc::new(SeatState::new());
        let mut state = Self {
            compositor: None,
            subcompositor: None,
            shm: None,
            shm_state: shm_state.clone(),
            seat: None,
            seat_state: seat_state.clone(),
            outputs: Default::default(),
        };

        let mut required_globals = protocols.unwrap_or_default();

        contents.with_list(|globals| {
            for global in globals.iter() {
                macro_rules! bind {
                    ($proto:tt, $objdata:expr, $state:expr) => {{
                        if let Some((x, proto)) =
                            protocol_bind(registry, global, &required_globals, $objdata)
                        {
                            if required_globals.extract_if_higher_protocol(&proto.into()) {
                                $state.replace(x);
                            }
                        }
                    }};
                }
                match &global.interface[..] {
                    // TODO: When stable: https://github.com/rust-lang/rust/pull/104087
                    "wl_compositor" => {
                        bind!(WlCompositor, Arc::new(EventlessState), state.compositor)
                    }
                    "wl_subcompositor" => bind!(
                        WlSubcompositor,
                        Arc::new(EventlessState),
                        state.subcompositor
                    ),
                    "wl_shm" => bind!(WlShm, shm_state.clone(), state.shm),
                    "wl_seat" => bind!(WlSeat, seat_state.clone(), state.seat),
                    "wl_output" => {
                        let output_state: Arc<OutputState> = Arc::new(Default::default());
                        if let Some((output, proto)) = protocol_bind::<WlOutput, _>(
                            registry,
                            global,
                            &required_globals,
                            output_state.clone(),
                        ) {
                            if required_globals.extract_if_higher_protocol(&proto.into()) {
                                state.outputs.insert(output, output_state.clone());
                            }
                        }
                    }
                    _ => (),
                }
            }
        });

        // Roundtrip to get SHM formats
        client.roundtrip()?;

        if !required_globals.is_empty() {
            return Err(WssyError::StateError(required_globals));
        }

        Ok(state)
    }
}

impl CoreState {
    pub fn shm_create_pool(&self, size: i32) -> Result<WlShmPool, InvalidId> {
        let file = memfd::MemfdOptions::default()
            .create("wayland shm pool")
            .unwrap();

        file.as_file().set_len(size as u64).unwrap();

        self.shm_create_pool_from_fd(&file, size)
    }

    pub fn shm_create_pool_from_fd<T: AsRawFd>(
        &self,
        fd: &T,
        size: i32,
    ) -> Result<WlShmPool, InvalidId> {
        let shm = self.shm.as_ref().unwrap();

        shm.send_constructor(
            wl_shm::Request::CreatePool {
                fd: unsafe { std::os::fd::BorrowedFd::borrow_raw(fd.as_raw_fd()) },
                size,
            },
            Arc::new(EventlessState),
        )
    }

    pub fn compositor_create_surface(
        &self,
        data: Arc<dyn ObjectData>,
    ) -> Result<WlSurface, InvalidId> {
        let compositor = self.compositor.as_ref().unwrap();
        compositor.send_constructor(wl_compositor::Request::CreateSurface {}, data)
    }

    #[instrument(skip(self), err, ret)]
    pub fn compositor_create_region(
        &mut self,
        x: i32,
        y: i32,
        width: i32,
        height: i32,
    ) -> Result<WlRegion, InvalidId> {
        trace!("Creating region");
        let compositor = self.compositor.as_ref().unwrap();
        compositor.send_constructor(
            wl_compositor::Request::CreateRegion {},
            Arc::new(WssyRegion::default()),
        )
    }

    pub fn subcompositor_get_subsurface(
        &self,
        surface: &WlSurface,
        parent: &WlSurface,
    ) -> Result<WlSubsurface, InvalidId> {
        let subcompositor = self.subcompositor.as_ref().unwrap();

        subcompositor.send_constructor(
            wl_subcompositor::Request::GetSubsurface {
                surface: surface.clone(),
                parent: parent.clone(),
            },
            Arc::new(EventlessState),
        )
    }

    pub fn seat_has_pointer(&self) -> bool {
        self.seat_state
            .capabilities
            .lock()
            .unwrap()
            .into_result()
            .is_ok_and(|c| c.contains(wl_seat::Capability::Pointer))
    }

    pub fn seat_has_touch(&self) -> bool {
        self.seat_state
            .capabilities
            .lock()
            .unwrap()
            .into_result()
            .is_ok_and(|c| c.contains(wl_seat::Capability::Touch))
    }

    pub fn seat_has_keyboard(&self) -> bool {
        self.seat_state
            .capabilities
            .lock()
            .unwrap()
            .into_result()
            .is_ok_and(|c| c.contains(wl_seat::Capability::Keyboard))
    }

    pub fn seat_get_pointer(&self, data: Arc<dyn ObjectData>) -> Result<WlPointer, InvalidId> {
        let seat = self.seat.as_ref().unwrap();

        seat.send_constructor(wl_seat::Request::GetPointer {}, data)
    }

    pub fn seat_get_keyboard(&self, data: Arc<dyn ObjectData>) -> Result<WlPointer, InvalidId> {
        let seat = self.seat.as_ref().unwrap();

        seat.send_constructor(wl_seat::Request::GetPointer {}, data)
    }

    pub fn seat_get_touch(&self, data: Arc<dyn ObjectData>) -> Result<WlTouch, InvalidId> {
        let seat = self.seat.as_ref().unwrap();

        seat.send_constructor(wl_seat::Request::GetTouch {}, data)
    }
}
