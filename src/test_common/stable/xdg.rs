//! Common functionality for XDG shell based testing.

use euclid::default::Point2D;
use std::{sync::Arc, time::Duration};
use wayland_backend::client::{InvalidId, ObjectData};
use wayland_client::{
    protocol::wl_surface::{self, WlSurface},
    Proxy,
};
use wayland_protocols::xdg::shell::client::{
    xdg_surface::{self, XdgSurface},
    xdg_toplevel::{self, XdgToplevel},
    xdg_wm_base::{self, XdgWmBase},
};

use crate::{
    harness::test_harness::Harness,
    test_common::{
        core::{BufferBuilder, CoreState},
        protocol_bind, Client, ClientState, CloneMessage, EventHandler, EventlessState, WssyError,
        WssyFuture, WssyRequiredProtocols,
    },
};

/// Representation of XDG state
#[derive(Debug, Default)]
struct WaylandXdgState(pub(crate) Option<XdgWmBase>);

pub struct XdgStableState {
    pub xdg_wm_base: Option<XdgWmBase>,
}

impl ClientState for XdgStableState {
    fn new(
        client: &mut Client,
        protocol: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError> {
        let registry = client.globals.registry();
        let contents = client.globals.contents();
        let mut state = Self { xdg_wm_base: None };
        let mut required_globals = protocol.unwrap_or_default();
        contents.with_list(|globals| {
            for global in globals.iter() {
                macro_rules! bind {
                    ($proto:tt, $objdata:expr, $state:expr) => {{
                        if let Some((x, proto)) =
                            protocol_bind(registry, global, &required_globals, $objdata)
                        {
                            if required_globals.extract_if_higher_protocol(&proto.into()) {
                                $state.replace(x);
                            }
                        }
                    }};
                }
                if &global.interface[..] == "xdg_wm_base" {
                    bind!(XdgWmBase, Arc::new(EventlessState), state.xdg_wm_base);
                }
            }
        });

        Ok(state)
    }
}

impl XdgStableState {
    pub fn get_xdg_surface(
        &self,
        surface: &WlSurface,
        data: Arc<dyn ObjectData>,
    ) -> Result<XdgSurface, InvalidId> {
        let wm_base = self.xdg_wm_base.as_ref().unwrap();
        wm_base.send_constructor(
            xdg_wm_base::Request::GetXdgSurface {
                surface: surface.clone(),
            },
            data,
        )
    }

    pub fn get_xdg_toplevel(
        &self,
        xdg_surface: &XdgSurface,
        data: Arc<dyn ObjectData>,
    ) -> Result<XdgToplevel, InvalidId> {
        xdg_surface.send_constructor(xdg_surface::Request::GetToplevel {}, data)
    }
}

/// A builder for toplevel surfaces
/// By default this acks the initial xdg_surface::configure
#[derive(Default)]
pub struct ToplevelBuilder {
    position: Option<Point2D<f64>>,
    surface_data: Option<Arc<dyn ObjectData>>,
    xdg_surface_data: Option<Arc<dyn ObjectData>>,
    xdg_toplevel_data: Option<Arc<dyn ObjectData>>,
}

impl ToplevelBuilder {
    pub fn new() -> Self {
        Self::default()
    }

    /// Move the toplevel to position once mapped
    pub fn with_position(mut self, position: impl Into<Point2D<f64>>) -> Self {
        self.position.replace(position.into());
        self
    }

    /// Set an ObjectData for the WlSurface messages to be passed to
    /// Messages are passed using CloneMessage after enter handler
    pub fn with_surface_object_data(mut self, od: Arc<impl ObjectData>) -> Self {
        self.surface_data.replace(od);
        self
    }

    /// Set an ObjectData for the XdgSurface messages to be passed to
    /// Messages are passed using CloneMessage after configure handler
    /// This overwrites the default ack_configure handler
    pub fn with_xdg_surface_object_data(mut self, od: Arc<impl ObjectData>) -> Self {
        self.xdg_surface_data.replace(od);
        self
    }

    /// Set ObjectData for XdgSurface to run provided closure on a
    /// xdg_surface::configure event This overwrites the default
    /// ack_configure handler
    pub fn with_xdg_surface_configure_handler<F: Fn(XdgSurface, u32) + Send + Sync + 'static>(
        mut self,
        cb: F,
    ) -> Self {
        self.xdg_surface_data
            .replace(EventHandler::new(
                move |xdg_surface: XdgSurface, event| match event {
                    xdg_surface::Event::Configure { serial } => cb(xdg_surface, serial),
                    _ => unreachable!(),
                },
            ));
        self
    }

    /// Set the ObjectData for the XdgToplevel
    pub fn with_xdg_toplevel_object_data(mut self, od: Arc<impl ObjectData>) -> Self {
        self.xdg_toplevel_data.replace(od);
        self
    }

    /// Create and maps a wl_surface, xdg_surface and xdg_toplevel.
    /// This does an initial commit, waits for the initial configure event,
    /// attaches the provided buffer, then waits for the enter event.
    pub fn map(
        self,
        harness: &impl Harness,
        client: &mut Client,
        configure_timeout: Duration,
        buffer_configure_fn: impl Fn(i32, i32, Vec<u8>) -> BufferBuilder,
    ) -> anyhow::Result<(WlSurface, XdgSurface, XdgToplevel)> {
        let core = client.state::<CoreState>();
        let xdg = client.state::<XdgStableState>();
        let surface_enter_future = WssyFuture::new();
        let xdg_surface_configure_future = WssyFuture::new();
        let xdg_toplevel_configure_future = WssyFuture::new();

        // Create wl_surface
        let surface_enter_handler = {
            let surface_enter_future = surface_enter_future.clone();
            EventHandler::<WlSurface>::new(move |_, event| {
                if let wl_surface::Event::Enter { .. } = event {
                    surface_enter_future.set(());
                }
            })
        };
        let surface_data: Arc<dyn ObjectData> = if let Some(surface_data) = self.surface_data {
            CloneMessage::new(vec![surface_enter_handler, surface_data])
        } else {
            surface_enter_handler
        };
        let surface = core.compositor_create_surface(surface_data)?;

        // Create xdg_surface
        let xdg_surface_configure_handler = {
            let xdg_surface_configure_future = xdg_surface_configure_future.clone();
            EventHandler::<XdgSurface>::new(move |_, event| {
                if let xdg_surface::Event::Configure { serial } = event {
                    xdg_surface_configure_future.set(serial);
                }
            })
        };
        let xdg_surface_data: Arc<dyn ObjectData> =
            if let Some(xdg_surface_data) = self.xdg_surface_data {
                CloneMessage::new(vec![xdg_surface_configure_handler, xdg_surface_data])
            } else {
                let initial_configure = WssyFuture::new();
                CloneMessage::new(vec![
                    xdg_surface_configure_handler,
                    EventHandler::<XdgSurface>::new(move |surface, event| {
                        if let xdg_surface::Event::Configure { serial } = event {
                            if initial_configure.get().is_none() {
                                surface.ack_configure(serial);
                                initial_configure.set(serial);
                            }
                        }
                    }),
                ])
            };
        let xdg_surface = xdg.get_xdg_surface(&surface, xdg_surface_data)?;

        // Create xdg_toplevel
        let xdg_toplevel_configure_handler = {
            let xdg_toplevel_configure_future = xdg_toplevel_configure_future.clone();
            EventHandler::<XdgToplevel>::new(move |_, event| {
                if let xdg_toplevel::Event::Configure {
                    width,
                    height,
                    states,
                } = event
                {
                    xdg_toplevel_configure_future.set((width, height, states));
                }
            })
        };
        let xdg_toplevel_data: Arc<dyn ObjectData> =
            if let Some(xdg_toplevel_data) = self.xdg_toplevel_data {
                CloneMessage::new(vec![xdg_toplevel_configure_handler, xdg_toplevel_data])
            } else {
                xdg_toplevel_configure_handler
            };
        let xdg_toplevel = xdg.get_xdg_toplevel(&xdg_surface, xdg_toplevel_data)?;

        // Commit for initial configure
        surface.commit();

        // Wait for xdg_surface::configure
        client.process_events_until(configure_timeout, || {
            xdg_surface_configure_future.get().is_some()
        })?;

        let (width, height, states) = xdg_toplevel_configure_future.get().unwrap_or_default();
        let buffer =
            buffer_configure_fn(width, height, states).build(client.state::<CoreState>())?;
        surface.attach(Some(&buffer), 0, 0);
        surface.commit();

        // Wait for surface to be mapped
        client.process_events_until(configure_timeout, || surface_enter_future.get().is_some())?;

        // Move surface to position
        if let Some(position) = self.position {
            harness.position_toplevel_absolute(
                &client.connection.backend(),
                &surface,
                position.x,
                position.y,
            );
        }

        Ok((surface, xdg_surface, xdg_toplevel))
    }
}
