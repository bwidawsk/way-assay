//! This is provided as a debug tool for testsuite integration.

use anyhow::Context;
pub use libloading::{Error as LibLoadingError, Library, Symbol};

use tracing_subscriber::filter::EnvFilter;
mod harness;
mod test_common;

fn setup_tracing() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("Couldn't set up tracing subscriber")
}

fn main() -> anyhow::Result<()> {
    setup_tracing()?;

    Ok(())
}
