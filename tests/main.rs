use std::sync::OnceLock;

use anyhow::Context;
use libtest_mimic::Arguments;

use tracing_subscriber::EnvFilter;
use way_assay::{harness, test_common::EarlyParameters};

use crate::config::build_config;

mod config;
mod tests;

fn setup_tracing() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .with_test_writer()
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("Couldn't set up tracing subscriber")
}

static EARLY_PARAMETERS: OnceLock<EarlyParameters> = OnceLock::new();

fn main() -> Result<(), anyhow::Error> {
    let args = Arguments::from_args();
    let mut tests = Vec::new();
    let params = EarlyParameters::new().context("Couldn't initialize parameters")?;
    let params = EARLY_PARAMETERS.get_or_init(|| params);
    if let Some(cfg) = params.cfg_path() {
        build_config(cfg).context("Failed to access config file")?;
    }

    #[cfg(feature = "wlcs")]
    let harness_manager = harness::wlcs::WlcsHarnessManager::new(params);
    #[cfg(not(feature = "wlcs"))]
    let harness_manager = harness::test_harness::NullHarnessManager;

    tests::wayland::add_tests(&mut tests, harness_manager.clone());
    #[cfg(feature = "xdg")]
    tests::xdg_shell::add_tests(&mut tests, harness_manager.clone());

    let _trace = setup_tracing();

    let conclusion = libtest_mimic::run(&args, tests);

    if !args.list {
        println!("{conclusion:?}");
        conclusion.exit_if_failed();
    }

    Ok(())
}
