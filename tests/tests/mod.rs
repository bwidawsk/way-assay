use libtest_mimic::Failed;
use way_assay::test_common::{Client, WssyError};
use wayland_backend::client::WaylandError;
use wayland_client::{DispatchError, Proxy};

pub mod wayland;
#[cfg(feature = "xdg")]
pub mod xdg_shell;

#[macro_export]
macro_rules! to_name {
    ($obj:ident) => {
        $obj::interface().name
    };
}

#[macro_export]
macro_rules! push_test {
    ($tests:ident, $harness_manager:ident, $fn:ident) => {
        let hm = $harness_manager.clone();
        let fn_name = format!(
            "{}::{}",
            module_path!().strip_prefix("wayassay::tests::").unwrap(),
            stringify!($fn)
        );
        $tests.push(Trial::skippable_test(fn_name, move || {
            let h = hm.create_harness()?;
            // Delay panic until harness is dropped to avoid stalling
            let r = std::panic::catch_unwind(std::panic::AssertUnwindSafe(|| $fn(h.clone())));
            hm.drop_harness(h)?;
            // This is taken from libtest_mimic
            r.unwrap_or_else(|e| {
                // The `panic` information is just an `Any` object representing the
                // value the panic was invoked with. For most panics (which use
                // `panic!` like `println!`), this is either `&str` or `String`.
                let payload = e
                    .downcast_ref::<String>()
                    .map(|s| s.as_str())
                    .or(e.downcast_ref::<&str>().map(|s| *s));

                let msg = match payload {
                    Some(payload) => format!("test panicked: {payload}"),
                    None => format!("test panicked"),
                };
                Err(Failed::from(msg))
            })
        }));
    };
}

#[macro_export]
macro_rules! to_client {
    ($state:expr) => {
        match $state {
            Ok(c) => c,
            Err(WssyError::StateError(x)) => return $crate::skip_trial!(&x.to_string()),
            Err(e) => return Err(e.into()),
        }
    };
}

#[macro_export]
macro_rules! skip_trial {
    ($reason:expr) => {
        Ok(Some($reason.to_string()))
    };
}

type LibtestReturn = Option<String>;

const fn _pass() -> LibtestReturn {
    None
}

/// Successful result for a Trial
const fn pass() -> Result<LibtestReturn, Failed> {
    Ok(_pass())
}

#[macro_export]
macro_rules! wssy_line {
    () => {
        format!("{}:{}:{}", file!(), line!(), column!())
    };
}

#[macro_export]
macro_rules! wssy_assert {
    ($cond:expr $(,)?) => {{
        if $cond {
            $crate::tests::pass()
        } else {
            Err(format!("assertion `{}` failed at {}", stringify!($cond), $crate::wssy_line!()).into())
        }
    }};
    ($cond:expr, $($arg:tt)+) => {{
        if $cond {
            $crate::tests::pass()
        } else {
            Err(format!("assertion `{}` failed with {:?} at {}", stringify!($cond), format!($($arg)*), $crate::wssy_line!()).into())
        }
    }};
}

#[macro_export]
macro_rules! wssy_assert_eq {
    ($left:expr, $right:expr $(,)?) => {
        if $left == $right {
            $crate::tests::pass()
        } else {
            Err(format!(
                "assertion `{}` failed at {}\n  left: {:?}\n right: {:?}",
                stringify!($left == $right),
                $crate::wssy_line!(),
                $left,
                $right,
            )
            .into())
        }
    };
    ($left:expr, $right:expr, $($arg:tt)+) => {
        if $left == $right {
            $crate::tests::pass()
        } else {
            Err(format!(
                "assertion `{}` failed with {:?} at {}\n  left: {:?}\n right: {:?}",
                stringify!($left == $right),
                format!($($arg)*),
                $crate::wssy_line!(),
                $left,
                $right,
            )
            .into())
        }
    };
}

/// Helper to turn bool into a Trial result
fn bool_result(result: bool, msg: &str) -> Result<LibtestReturn, Failed> {
    result.then_some(_pass()).ok_or(Failed::from(msg))
}

/// Helper to turn roundtrip into a Trial result.
fn roundtrip_result(client: &mut Client) -> Result<LibtestReturn, Failed> {
    client.roundtrip().map(|_| _pass()).map_err(Failed::from)
}

fn expect_failure(res: Result<usize, WssyError>, msg: &str) -> Result<LibtestReturn, Failed> {
    match res {
        Ok(_) => Err(Failed::from(msg)),
        Err(_) => pass(),
    }
}

fn expect_failure_code<P: Proxy, C: std::fmt::Debug + Into<u32> + Copy>(
    res: Result<usize, WssyError>,
    code: C,
    location: String,
) -> Result<LibtestReturn, Failed> {
    let name = P::interface().name;
    let expected = format!(
        "expected protocol error code {} ({code:?}) on {name}",
        code.into()
    );
    match res {
        Ok(_) => Err(Failed::from(format!("No error, {expected} at {location}"))),
        Err(WssyError::DispatchError(error)) => match error {
            DispatchError::Backend(error) => match error {
                WaylandError::Io(error) => Err(Failed::from(format!(
                    "IO error {error}, {expected} at {location}"
                ))),
                WaylandError::Protocol(error) => {
                    if error.object_interface == name && error.code == code.into() {
                        pass()
                    } else {
                        Err(Failed::from(format!(
                            "Received code {} on {}@{}, {expected} at {location}",
                            error.code, error.object_interface, error.object_id
                        )))
                    }
                }
            },
            _ => Err(Failed::from(format!(
                "Unexpected error {error}, {expected} at {location}"
            ))),
        },

        Err(error) => Err(Failed::from(format!(
            "Unexpected error {error}, {expected} at {location}"
        ))),
    }
}

#[macro_export]
macro_rules! expect_failure_code {
    ($res:expr, $proto:ty, $code:expr $(,)?) => {{
        $crate::tests::expect_failure_code::<$proto, _>($res, $code, $crate::wssy_line!())
    }};
}
