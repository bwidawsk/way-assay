use std::sync::Arc;

use libtest_mimic::{Failed, Trial};
use way_assay::{
    harness::test_harness::{Harness, HarnessManager},
    test_common::{
        core::{BufferBuilder, CoreState},
        stable::xdg::XdgStableState,
        Client, EventHandler, EventlessState, WssyFuture,
    },
};
use wayland_protocols::xdg::shell::client::xdg_surface::{self, XdgSurface};

use crate::{
    config::configure_timeout,
    expect_failure_code, push_test,
    tests::{roundtrip_result, LibtestReturn},
};

// "Creating an xdg_surface from a wl_surface which has a buffer attached or
// committed is a client error, and any attempts by a client to attach or
// manipulate a buffer prior to the first xdg_surface.configure call must also
// be treated as errors."
fn xdg_surface_disallows_buffer_before_first_configure<H: Harness>(
    harness: H,
) -> Result<LibtestReturn, Failed> {
    let mut client = Client::new(harness)
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;
    let core = client.state::<CoreState>();
    let xdg = client.state::<XdgStableState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let _xdg_surface = xdg.get_xdg_surface(&surface, Arc::new(EventlessState))?;
    let buffer = BufferBuilder::new(32, 32).build(&core)?;
    surface.attach(Some(&buffer), 0, 0);
    surface.commit();

    expect_failure_code!(
        client.roundtrip(),
        XdgSurface,
        xdg_surface::Error::UnconfiguredBuffer
    )
}

fn xdg_toplevel_allows_buffer_after_first_configure<H: Harness>(
    harness: H,
) -> Result<LibtestReturn, Failed> {
    let mut client = Client::new(harness)
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;
    let core = client.state::<CoreState>();
    let xdg = client.state::<XdgStableState>();
    let serial = WssyFuture::new();
    let s = serial.clone();

    let xdg_surface_handler =
        EventHandler::<XdgSurface>::new(move |xdg_surface, event| match event {
            xdg_surface::Event::Configure { serial } => {
                xdg_surface.ack_configure(serial);
                s.set(serial);
            }
            _ => unreachable!(),
        });

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let xdg_surface = xdg.get_xdg_surface(&surface, xdg_surface_handler)?;
    let _xdg_toplevel = xdg.get_xdg_toplevel(&xdg_surface, Arc::new(EventlessState))?;

    // Commiting without a buffer after the role is assigned should kick off the
    // configure event.
    surface.commit();
    roundtrip_result(&mut client)?;

    client.process_events_until(configure_timeout(), || serial.get().is_some())?;

    // Reget core state to appease the borrow checker
    let core = client.state::<CoreState>();
    let buffer = BufferBuilder::new(32, 32).build(&core)?;
    surface.attach(Some(&buffer), 0, 0);
    surface.commit();

    roundtrip_result(&mut client)
}

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    push_test!(
        tests,
        harness_manager,
        xdg_surface_disallows_buffer_before_first_configure
    );
    push_test!(
        tests,
        harness_manager,
        xdg_toplevel_allows_buffer_after_first_configure
    );
}
