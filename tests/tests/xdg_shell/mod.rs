use libtest_mimic::Trial;
use way_assay::harness::test_harness::HarnessManager;

mod basic;

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    basic::add_tests(tests, harness_manager);
}
