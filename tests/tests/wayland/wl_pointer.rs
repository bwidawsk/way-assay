use std::sync::{Arc, RwLock};

use euclid::{
    default::{Rect, Size2D},
    rect, size2,
};
use libtest_mimic::{Failed, Trial};

use way_assay::{
    harness::test_harness::{Harness, HarnessManager},
    test_common::{
        core::{BufferBuilder, CoreState},
        stable::xdg::{ToplevelBuilder, XdgStableState},
        Client,
    },
};

use wayland_backend::client::ObjectData;
use wayland_client::{
    protocol::{
        wl_pointer::{self, WlPointer},
        wl_surface::WlSurface,
    },
    Connection, Proxy,
};

use crate::{config::configure_timeout, push_test, tests::pass, wssy_assert, wssy_assert_eq};

use crate::tests::LibtestReturn;

// TODO: Import values from linux/input-event-codes.h
const BTN_MOUSE: u32 = 0x110;

type PointerLocation = Option<(WlSurface, f64, f64)>;

#[derive(Default, Clone)]
struct PointerData {
    // If target_surface is set count enter/leaves for it otherwise count any surface
    target_surface: Option<WlSurface>,
    enter_count: u32,
    leave_count: u32,

    pressed_buttons: Vec<u32>,
    location: PointerLocation,
}

impl PointerData {
    fn new() -> Arc<RwLock<Self>> {
        Default::default()
    }

    fn new_with_surface(surface: WlSurface) -> Arc<RwLock<Self>> {
        Arc::new(RwLock::new(Self {
            target_surface: Some(surface),
            ..Default::default()
        }))
    }
}

struct PointerObject(Arc<RwLock<PointerData>>);

impl PointerObject {
    fn new(data: Arc<RwLock<PointerData>>) -> Arc<Self> {
        Arc::new(Self(data))
    }
}

impl ObjectData for PointerObject {
    fn event(
        self: Arc<Self>,
        backend: &wayland_backend::client::Backend,
        msg: wayland_backend::protocol::Message<
            wayland_backend::client::ObjectId,
            std::os::unix::prelude::OwnedFd,
        >,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((_, event)) = WlPointer::parse_event(&conn, msg) else {
            //error!("Bad pointer");
            unreachable!()
        };

        use wl_pointer::{ButtonState, Event};
        let mut data = self.0.write().unwrap();
        match event {
            Event::Enter {
                surface,
                surface_x,
                surface_y,
                ..
            } => {
                data.location = Some((surface.clone(), surface_x, surface_y));
                if let Some(target_surface) = data.target_surface.as_ref() {
                    if *target_surface == surface {
                        data.enter_count += 1;
                    }
                } else {
                    data.enter_count += 1;
                }
            }
            Event::Leave { surface, .. } => {
                if let Some((current_surface, ..)) = data.location.as_ref() {
                    // Check that we're not leaving a destroyed surface
                    if surface.is_alive() {
                        wssy_assert_eq!(*current_surface, surface).unwrap();
                    }
                    data.location = None;
                }
                if let Some(target_surface) = data.target_surface.as_ref() {
                    if *target_surface == surface {
                        data.leave_count += 1;
                    }
                } else {
                    data.leave_count += 1;
                }
            }
            Event::Motion {
                surface_x,
                surface_y,
                ..
            } => {
                if let Some((_, x, y)) = data.location.as_mut() {
                    *x = surface_x;
                    *y = surface_y;
                }
            }
            Event::Button { button, state, .. } => {
                if let Ok(state) = state.into_result() {
                    match state {
                        ButtonState::Pressed => {
                            data.pressed_buttons.push(button);
                        }
                        ButtonState::Released => {
                            data.pressed_buttons.retain(|&b| button != b);
                        }
                        _ => (),
                    };
                }
            }
            _ => (),
        }

        None
    }

    fn destroyed(&self, _object_id: wayland_backend::client::ObjectId) {}
}

fn move_surface_relative(
    harness: &impl Harness,
    client: &Client,
    surface: &WlSurface,
    surface_rect: &mut Rect<f64>,
    deviation: impl Into<Size2D<f64>>,
) -> Result<(), Failed> {
    surface_rect.origin += deviation.into();
    harness.position_toplevel_absolute(
        &client.connection.backend(),
        &surface,
        surface_rect.origin.x,
        surface_rect.origin.y,
    );
    Ok(())
}

// ⌜⌝
// ⌞⌟
fn corners<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;
    let surface_rect = rect(46., 76., 111., 134.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new_with_surface(surface.clone());
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    surface.commit();
    client.roundtrip()?;

    // Top Left ⌜
    let mut pointer_loc = surface_rect.origin;
    pointer_loc -= size2(1., 1.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);
    for i in 1..3 {
        // Enter
        pointer_loc += size2(1., 1.);
        pointer.move_relative(1., 1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(
            surface_rect.contains(pointer_loc),
            "Surface will not overlap"
        )?;
        wssy_assert_eq!(data.read()?.enter_count, i, "Enter count wrong")?;

        // Leave
        pointer_loc += size2(-1., -1.);
        pointer.move_relative(-1., -1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(!surface_rect.contains(pointer_loc), "Surface will overlap")?;
        wssy_assert_eq!(data.read()?.leave_count, i, "Leave count wrong")?;
    }

    // Top Right ⌝
    let mut pointer_loc = surface_rect.origin;
    pointer_loc += size2(surface_rect.size.width as f64, -1.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);
    {
        let mut data = data.write()?;
        data.enter_count = 0;
        data.leave_count = 0;
    }
    for i in 1..3 {
        // Enter
        pointer_loc += size2(-1., 1.);
        pointer.move_relative(-1., 1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(
            surface_rect.contains(pointer_loc),
            "Surface will not overlap"
        )?;
        wssy_assert_eq!(data.read()?.enter_count, i, "Enter count wrong")?;

        // Leave
        pointer_loc += size2(1., -1.);
        pointer.move_relative(1., -1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(!surface_rect.contains(pointer_loc), "Surface will overlap")?;
        wssy_assert_eq!(data.read()?.leave_count, i, "Leave count wrong")?;
    }

    // Bottom Left ⌞
    let mut pointer_loc = surface_rect.origin;
    pointer_loc += size2(surface_rect.size.width as f64, -1.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);
    {
        let mut data = data.write()?;
        data.enter_count = 0;
        data.leave_count = 0;
    }
    for i in 1..3 {
        // Enter
        pointer_loc += size2(-1., 1.);
        pointer.move_relative(-1., 1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(
            surface_rect.contains(pointer_loc),
            "Surface will not overlap"
        )?;
        wssy_assert_eq!(data.read()?.enter_count, i, "Enter count wrong")?;

        // Leave
        pointer_loc += size2(1., -1.);
        pointer.move_relative(1., -1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(!surface_rect.contains(pointer_loc), "Surface will overlap")?;
        wssy_assert_eq!(data.read()?.leave_count, i, "Leave count wrong")?;
    }

    // Bottom Right ⌟
    let mut pointer_loc = surface_rect.origin;
    pointer_loc += size2(
        surface_rect.size.width as f64,
        surface_rect.size.height as f64,
    );
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);
    {
        let mut data = data.write()?;
        data.enter_count = 0;
        data.leave_count = 0;
    }
    for i in 1..3 {
        // Enter
        pointer_loc += size2(-1., -1.);
        pointer.move_relative(-1., -1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(
            surface_rect.contains(pointer_loc),
            "Surface will not overlap"
        )?;
        wssy_assert_eq!(data.read()?.enter_count, i, "Enter count wrong")?;

        // Leave
        pointer_loc += size2(1., 1.);
        pointer.move_relative(1., 1.);
        client.roundtrip()?;
        // Verify
        wssy_assert!(!surface_rect.contains(pointer_loc), "Surface will overlap")?;
        wssy_assert_eq!(data.read()?.leave_count, i, "Leave count wrong")?;
    }

    pass()
}

fn button_press_each<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let surface_rect = rect(100., 100., 100., 100.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new();
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    surface.commit();
    client.roundtrip()?;

    let pointer_loc = surface_rect.origin;
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);

    // Range of BTN_LEFT to BTN_TASK
    let button_range = BTN_MOUSE..=(BTN_MOUSE + 7);
    // Press buttons
    button_range
        .clone()
        .for_each(|button| pointer.button_press(button));
    client.roundtrip()?;

    // Check that button pressed events were recieved in order
    wssy_assert_eq!(
        data.read()?.pressed_buttons,
        button_range.clone().collect::<Vec<_>>(),
        "Pressed buttons do not match"
    )?;

    // Release buttons
    button_range
        .rev()
        .for_each(|button| pointer.button_release(button));
    client.roundtrip()?;

    wssy_assert!(
        data.read()?.pressed_buttons.is_empty(),
        "Buttons still pressed"
    )?;

    pass()
}

fn surface_moved_under_pointer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let mut surface_rect = rect(100., 100., 100., 100.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new_with_surface(surface.clone());
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    surface.commit();
    client.roundtrip()?;

    // Place pointer 10px diagonally off the surface
    let mut pointer_loc = surface_rect.origin;
    pointer_loc -= size2(10., 10.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);

    wssy_assert!(!surface_rect.contains(pointer_loc), "Surface will overlap")?;
    wssy_assert_eq!(data.read()?.enter_count, 0, "Enter count wrong")?;

    // Move the surface 20px diagonally to be under the pointer
    surface_rect.origin -= size2(20., 20.);
    harness.position_toplevel_absolute(
        &client.connection.backend(),
        &surface,
        surface_rect.origin.x,
        surface_rect.origin.y,
    );

    client.roundtrip()?;

    wssy_assert!(
        surface_rect.contains(pointer_loc),
        "Surface will not overlap"
    )?;
    wssy_assert_eq!(data.read()?.enter_count, 1, "Enter count wrong")?;

    pass()
}

fn surface_destroyed_under_pointer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let surface_rect = rect(100., 100., 100., 100.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new();
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    surface.commit();
    client.roundtrip()?;

    // Move the pointer onto the surface
    let mut pointer_loc = surface_rect.origin;
    pointer_loc += size2(10., 10.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);

    client.roundtrip()?;
    wssy_assert!(
        surface_rect.contains(pointer_loc),
        "Surface does not overlap"
    )?;
    wssy_assert_eq!(data.read()?.enter_count, 1, "Enter count wrong")?;

    // Destroy surface and expect a leave
    surface.destroy();
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.leave_count, 1, "Leave count wrong")?;

    pass()
}

fn enter_on_fullscreen<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let surface_rect = rect(100., 100., 100., 100.);

    let (surface, _, xdg_toplevel) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new();
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    surface.commit();
    client.roundtrip()?;

    // Move the pointer onto the surface
    let mut pointer_loc = surface_rect.origin;
    pointer_loc -= size2(10., 10.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);

    client.roundtrip()?;
    wssy_assert!(!surface_rect.contains(pointer_loc), "Surface overlaps")?;
    wssy_assert_eq!(data.read()?.enter_count, 0, "Enter count wrong")?;

    // Set toplevel fullscreen and expect an enter
    xdg_toplevel.set_fullscreen(None);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.enter_count, 1, "Enter count wrong")?;

    pass()
}

fn enter_when_uncovered<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    // Create a toplevel then a second to cover the it
    let back_rect = rect(110., 110., 80., 80.);
    let front_rect = rect(100., 100., 100., 100.);

    let (back_surface, _, _) = ToplevelBuilder::new().with_position(back_rect.origin).map(
        &harness,
        &mut client,
        configure_timeout(),
        |_, _, _| BufferBuilder::new(back_rect.size.width as i32, back_rect.size.height as i32),
    )?;

    let (front_surface, _, _) = ToplevelBuilder::new()
        .with_position(front_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(front_rect.size.width as i32, front_rect.size.height as i32)
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new_with_surface(back_surface.clone());
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    // Move the pointer onto the surface
    let mut pointer_loc = back_rect.origin;
    pointer_loc += size2(10., 10.);
    pointer.move_absolute(pointer_loc.x, pointer_loc.y);
    client.roundtrip()?;

    wssy_assert_eq!(data.read()?.enter_count, 0, "Enter count wrong")?;

    front_surface.destroy();
    client.roundtrip()?;

    wssy_assert_eq!(data.read()?.enter_count, 1, "Enter count wrong")?;

    pass()
}

fn surface_coords<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let surface_rect = rect(100., 100., 100., 100.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new();
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    // Position pointer off surface top left
    pointer.move_absolute(surface_rect.origin.x - 1., surface_rect.origin.y - 1.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, PointerLocation::None)?;

    // Position pointer on surface top left
    pointer.move_relative(1., 1.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 0., 0.)))?;

    // Move pointer diagonally
    pointer.move_relative(10., 10.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 10., 10.)))?;

    // Move pointer right by a unit
    pointer.move_relative(1., 0.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 11., 10.)))?;

    // Move pointer to bottom right
    pointer.move_relative(84., 85.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 95., 95.)))?;

    pass()
}

fn surface_coords_fractional_unit<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let surface_rect = rect(100., 100., 100., 100.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new();
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    // Position pointer off surface top left
    pointer.move_absolute(surface_rect.origin.x - 0.5, surface_rect.origin.y - 0.5);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, PointerLocation::None)?;

    // Position pointer on surface top left
    pointer.move_relative(1., 1.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 0.5, 0.5)))?;

    // Move pointer diagonally
    pointer.move_relative(10., 10.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 10.5, 10.5)))?;

    // Move pointer a fraction right/down
    pointer.move_relative(0.5, 0.5);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 11., 11.)))?;

    pass()
}

fn surface_coords_move_surface<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let pointer = harness.create_pointer();
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let mut surface_rect = rect(111., 111., 100., 100.);

    let (surface, _, _) = ToplevelBuilder::new()
        .with_position(surface_rect.origin)
        .map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(
                surface_rect.size.width as i32,
                surface_rect.size.height as i32,
            )
        })?;

    let core = client.state::<CoreState>();
    wssy_assert!(core.seat_has_pointer(), "Seat needs pointer capability")?;

    let data = PointerData::new();
    let _pointer = core.seat_get_pointer(PointerObject::new(data.clone()));

    // Position pointer off surface top left
    pointer.move_absolute(110., 110.);
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, PointerLocation::None)?;

    // Position top left of surface under pointer
    move_surface_relative(&harness, &client, &surface, &mut surface_rect, (-1., -1.))?;
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 0., 0.)))?;

    // Move surface diagonally
    move_surface_relative(&harness, &client, &surface, &mut surface_rect, (-10., -10.))?;
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 10., 10.)))?;

    // Move surface right by a unit
    move_surface_relative(&harness, &client, &surface, &mut surface_rect, (-1., 0.))?;
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 11., 10.)))?;

    // Move surface up by a unit
    move_surface_relative(&harness, &client, &surface, &mut surface_rect, (0., 1.))?;
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 11., 9.)))?;

    // Move bottom right of surface under pointer
    move_surface_relative(
        &harness,
        &client,
        &surface,
        &mut surface_rect,
        (-84.0, -86.0),
    )?;
    client.roundtrip()?;
    wssy_assert_eq!(data.read()?.location, Some((surface.clone(), 95., 95.)))?;

    pass()
}

pub fn add_tests(tests: &mut Vec<Trial>, harness: impl HarnessManager) {
    push_test!(tests, harness, corners);
    push_test!(tests, harness, button_press_each);
    push_test!(tests, harness, surface_moved_under_pointer);
    push_test!(tests, harness, surface_destroyed_under_pointer);
    push_test!(tests, harness, enter_on_fullscreen);
    push_test!(tests, harness, enter_when_uncovered);
    push_test!(tests, harness, surface_coords);
    push_test!(tests, harness, surface_coords_fractional_unit);
    push_test!(tests, harness, surface_coords_move_surface);
}
