use libtest_mimic::Trial;
use way_assay::harness::test_harness::HarnessManager;

mod misc;
mod wl_pointer;
mod wl_shm;
mod wl_subsurface;
mod wl_surface;

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    misc::add_tests(tests, harness_manager.clone());
    wl_pointer::add_tests(tests, harness_manager.clone());
    wl_shm::add_tests(tests, harness_manager.clone());
    wl_surface::add_tests(tests, harness_manager.clone());
    wl_subsurface::add_tests(tests, harness_manager.clone());
}
