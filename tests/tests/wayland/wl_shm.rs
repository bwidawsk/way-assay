use libtest_mimic::{Failed, Trial};

use way_assay::{
    harness::test_harness::{Harness, HarnessManager},
    test_common::{
        core::{BufferBuilder, CoreState},
        Client, WssyError,
    },
};

use wayland_client::protocol::{wl_shm, wl_shm_pool::WlShmPool};

use crate::{
    expect_failure_code, push_test, skip_trial,
    tests::{bool_result, roundtrip_result, LibtestReturn},
    to_client, wssy_assert_eq,
};

fn pool_stride_format_mismatch<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let _buffer = BufferBuilder::new(1, 2)
        .with_format(3, wl_shm::Format::Argb8888)
        .build(&core);

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidStride)
}

fn pool_stride_out_of_bounds<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let pool = core.shm_create_pool(32)?;
    let _buffer = BufferBuilder::new(8, 1)
        .with_pool(&pool, 0)
        .with_format(36, wl_shm::Format::Argb8888)
        .build(&core)?;

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidStride)
}

fn pool_size_out_of_bounds<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let pool = core.shm_create_pool(32)?;
    let _buffer = BufferBuilder::new(3, 3)
        .with_pool(&pool, 0)
        .with_format(12, wl_shm::Format::Argb8888)
        .build(&core)?;

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidStride)
}

fn pool_offset_pushes_out_of_bounds<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let pool = core.shm_create_pool(32)?;
    let _buffer = BufferBuilder::new(8, 1)
        .with_pool(&pool, 1)
        .with_format(32, wl_shm::Format::Argb8888)
        .build(&core)?;

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidStride)
}

// wl_shm.format:
// > All renderers should support argb8888 and xrgb8888 but any other
// > formats are optional and may not be supported by the particular
// > renderer in use.
fn formats_contain_8888<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let formats = core.shm_state.formats.lock()?;

    bool_result(
        formats.contains(&wl_shm::Format::Argb8888),
        "Missing ARGB8888",
    )?;
    bool_result(
        formats.contains(&wl_shm::Format::Xrgb8888),
        "Missing XRGB8888",
    )
}

fn pool_create_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let _buffer = BufferBuilder::new(8, 8).build(&core)?;

    roundtrip_result(&mut client)
}

fn pool_cannot_resize_smaller<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let size = 32;
    let file = memfd::MemfdOptions::default()
        .create("wayland shm pool")
        .unwrap();
    file.as_file().set_len(size as u64).unwrap();

    let pool = core.shm_create_pool_from_fd(&file, size)?;

    let size = 16;
    file.as_file().set_len(size as u64)?;
    pool.resize(size);

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidFd)
}

fn pool_resize_larger<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let size = 32;
    let file = memfd::MemfdOptions::default()
        .create("wayland shm pool")
        .unwrap();
    file.as_file().set_len(size as u64).unwrap();

    let pool = core.shm_create_pool_from_fd(&file, size)?;

    let size = 64;
    file.as_file().set_len(size as u64)?;
    pool.resize(size);

    roundtrip_result(&mut client)
}

// wl_shm: create_pool
// > The server will mmap size bytes of the passed file descriptor, to use as
// > backing memory for the pool.
fn pool_creation_fail_if_size_larger_than_backing_storage<H>(
    harness: H,
) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let file = memfd::MemfdOptions::default()
        .create("wayland shm pool")
        .unwrap();

    wssy_assert_eq!(
        file.as_file().metadata().unwrap().len(),
        0,
        "Expected 0 sized backing store"
    )?;

    let _pool = core.shm_create_pool_from_fd(&file, 256)?;

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidFd)
}

fn pool_invalid_format<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();
    let maybe_unsupported = [
        wl_shm::Format::Axbxgxrx106106106106,
        wl_shm::Format::Y416,
        wl_shm::Format::Y0l0,
    ];
    let formats = core.shm_state.formats.lock()?;
    let unsupported = match maybe_unsupported
        .iter()
        .find(move |fmt| !formats.contains(fmt))
    {
        Some(fmt) => *fmt,
        None => return skip_trial!("Unsupported format not available."),
    };

    let _buffer = BufferBuilder::new(1, 1)
        .with_format(64, unsupported)
        .build(&core)?;

    expect_failure_code!(client.roundtrip(), WlShmPool, wl_shm::Error::InvalidFormat)
}

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    push_test!(tests, harness_manager, pool_stride_format_mismatch);
    push_test!(tests, harness_manager, pool_stride_out_of_bounds);
    push_test!(tests, harness_manager, pool_size_out_of_bounds);
    push_test!(tests, harness_manager, pool_offset_pushes_out_of_bounds);
    push_test!(tests, harness_manager, formats_contain_8888);
    push_test!(tests, harness_manager, pool_create_buffer);
    push_test!(tests, harness_manager, pool_cannot_resize_smaller);
    push_test!(tests, harness_manager, pool_resize_larger);
    push_test!(
        tests,
        harness_manager,
        pool_creation_fail_if_size_larger_than_backing_storage
    );
    push_test!(tests, harness_manager, pool_invalid_format);
}
