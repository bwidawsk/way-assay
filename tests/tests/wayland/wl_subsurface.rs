use std::sync::Arc;

use libtest_mimic::{Failed, Trial};

use way_assay::{
    harness::test_harness::{Harness, HarnessManager},
    test_common::{core::CoreState, Client, EventlessState, WssyError},
};

use wayland_backend::client::InvalidId;
use wayland_client::protocol::{
    wl_subcompositor::{self, WlSubcompositor},
    wl_subsurface::{self, WlSubsurface},
    wl_surface::WlSurface,
};

use crate::{
    expect_failure_code, push_test,
    tests::{roundtrip_result, LibtestReturn},
    to_client,
};

#[derive(Debug)]
struct SubsurfacePair {
    surf: WlSurface,
    subsurf: WlSubsurface,
}

/// Create a surface and add N subsurfaces to it.
fn populate_compound_surface<const N: usize>(
    core: &CoreState,
) -> Result<(WlSurface, [SubsurfacePair; N]), Failed> {
    let parent = core.compositor_create_surface(Arc::new(EventlessState))?;

    // try_from_fn can be used to create fixed size array or error.
    // However it is unstable: https://github.com/rust-lang/rust/issues/89379
    // let sub_surface_pairs = std::array::try_from_fn(|_| {
    //     let surf = core.compositor_create_surface()?;
    //     let subsurf = core.subcompositor_get_subsurface(&surf, &parent)?;
    //     Ok::<SubsurfacePair, InvalidId>(SubsurfacePair { surf, subsurf })
    // })?;

    let sub_surface_pairs = {
        let pairs = std::iter::repeat_with(|| {
            let surf = core.compositor_create_surface(Arc::new(EventlessState))?;
            let subsurf = core.subcompositor_get_subsurface(&surf, &parent)?;
            Ok(SubsurfacePair { surf, subsurf })
        })
        .take(N)
        .collect::<Result<Vec<_>, InvalidId>>()?;

        <[SubsurfacePair; N]>::try_from(pairs).unwrap()
    };

    Ok((parent, sub_surface_pairs))
}

fn creation<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let _compound_a = populate_compound_surface::<1>(core)?;
    let _compound_b = populate_compound_surface::<3>(core)?;
    let _compound_c = populate_compound_surface::<5>(core)?;

    roundtrip_result(&mut client)
}

fn positioning<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<3>(core)?;

    for (i, child) in children.iter().enumerate() {
        let p = (i as i32 + 2) * 20;
        child.subsurf.set_position(p, p);
    }

    roundtrip_result(&mut client)
}

fn placement<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (parent, children) = populate_compound_surface::<3>(core)?;

    children[0].subsurf.place_above(&children[1].surf);
    children[1].subsurf.place_above(&parent);
    children[2].subsurf.place_below(&children[0].surf);
    children[1].subsurf.place_below(&parent);

    roundtrip_result(&mut client)
}

fn cannot_become_own_parent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let parent = core.compositor_create_surface(Arc::new(EventlessState))?;

    /* surface is its own parent */
    let _sub = core.subcompositor_get_subsurface(&parent, &parent);

    expect_failure_code!(
        client.roundtrip(),
        WlSubcompositor,
        wl_subcompositor::Error::BadSurface,
    )
}

fn cannot_reparent_to_parent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (parent, children) = populate_compound_surface::<3>(core)?;

    /* surface is already a subsurface */
    let _sub = core.subcompositor_get_subsurface(&children[0].surf, &parent);

    expect_failure_code!(
        client.roundtrip(),
        WlSubcompositor,
        wl_subcompositor::Error::BadSurface,
    )
}

fn cannot_reparent_to_stranger<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let stranger = core.compositor_create_surface(Arc::new(EventlessState))?;
    let (_parent, children) = populate_compound_surface::<3>(core)?;

    /* surface is already a subsurface */
    let _sub = core.subcompositor_get_subsurface(&children[0].surf, &stranger)?;

    expect_failure_code!(
        client.roundtrip(),
        WlSubcompositor,
        wl_subcompositor::Error::BadSurface,
    )
}

fn nesting_child_becomes_parent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let stranger = core.compositor_create_surface(Arc::new(EventlessState))?;
    let (_parent, children) = populate_compound_surface::<1>(core)?;

    /* parent is a sub-surface */
    let _sub = core.subcompositor_get_subsurface(&stranger, &children[0].surf)?;

    roundtrip_result(&mut client)
}

fn nesting_parent_becomes_child<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let stranger = core.compositor_create_surface(Arc::new(EventlessState))?;
    let (parent, _children) = populate_compound_surface::<3>(core)?;

    /* surface is already a parent */
    let _sub = core.subcompositor_get_subsurface(&parent, &stranger)?;

    roundtrip_result(&mut client)
}

fn cannot_create_looping_hierarchy<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = [
        core.compositor_create_surface(Arc::new(EventlessState))?,
        core.compositor_create_surface(Arc::new(EventlessState))?,
        core.compositor_create_surface(Arc::new(EventlessState))?,
    ];

    /* create a nesting loop */
    let _subs = [
        core.subcompositor_get_subsurface(&surface[1], &surface[0])?,
        core.subcompositor_get_subsurface(&surface[2], &surface[1])?,
        /* can't make a surface its own grandparent */
        core.subcompositor_get_subsurface(&surface[0], &surface[2])?,
    ];

    expect_failure_code!(
        client.roundtrip(),
        WlSubcompositor,
        wl_subcompositor::Error::BadSurface,
    )
}

fn place_above_nested_parent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<1>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    sub.place_above(&children[0].surf);

    roundtrip_result(&mut client)
}

fn place_below_nested_parent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<1>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    sub.place_below(&children[0].surf);

    roundtrip_result(&mut client)
}

fn cannot_place_above_grandparent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (parent, children) = populate_compound_surface::<1>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    /* can't place a subsurface above its grandparent */
    sub.place_above(&parent);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_above_parents_sibling<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<2>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    /* can't place a subsurface above its parents' siblings */
    sub.place_above(&children[1].surf);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_above_child<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<1>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let _sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    /* can't place a subsurface above its own child subsurface */
    children[0].subsurf.place_above(&grandchild);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_below_grandparent<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (parent, children) = populate_compound_surface::<1>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    /* can't place a subsurface below its grandparent */
    sub.place_below(&parent);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_below_parents_sibling<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<2>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    /* can't place a subsurface below its parents' siblings */
    sub.place_below(&children[1].surf);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_below_child<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent, children) = populate_compound_surface::<1>(core)?;

    let grandchild = core.compositor_create_surface(Arc::new(EventlessState))?;
    let _sub = core.subcompositor_get_subsurface(&grandchild, &children[0].surf)?;

    /* can't place a subsurface below its own child subsurface */
    children[0].subsurf.place_below(&grandchild);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_above_stranger<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let stranger = core.compositor_create_surface(Arc::new(EventlessState))?;
    let (_parent, children) = populate_compound_surface::<1>(core)?;

    /* can't define order with unrelated surface */
    children[0].subsurf.place_above(&stranger);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_below_stranger<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let stranger = core.compositor_create_surface(Arc::new(EventlessState))?;
    let (_parent, children) = populate_compound_surface::<1>(core)?;

    /* can't define order with unrelated surface */
    children[0].subsurf.place_below(&stranger);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_above_across_trees<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent_a, children_a) = populate_compound_surface::<1>(core)?;
    let (_parent_b, children_b) = populate_compound_surface::<1>(core)?;

    /* can't define order with unrelated surface */
    children_a[0].subsurf.place_above(&children_b[0].surf);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn cannot_place_below_across_trees<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (_parent_a, children_a) = populate_compound_surface::<1>(core)?;
    let (_parent_b, children_b) = populate_compound_surface::<1>(core)?;

    /* can't define order with unrelated surface */
    children_a[0].subsurf.place_below(&children_b[0].surf);

    expect_failure_code!(
        client.roundtrip(),
        WlSubsurface,
        wl_subsurface::Error::BadSurface
    )
}

fn destruction<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let (parent, children) = populate_compound_surface::<3>(core)?;

    /* not needed anymore */
    core.subcompositor.as_ref().unwrap().destroy();

    /* detach child from parent */
    children[0].subsurf.destroy();

    /* destroy: child, parent */
    children[1].surf.destroy();
    parent.destroy();

    /* destroy: parent, child */
    children[2].surf.destroy();

    /* destroy: sub, child */
    children[0].surf.destroy();

    /* 2x destroy: child, sub */
    children[2].subsurf.destroy();
    children[1].subsurf.destroy();

    roundtrip_result(&mut client)
}

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    push_test!(tests, harness_manager, creation);
    push_test!(tests, harness_manager, positioning);
    push_test!(tests, harness_manager, placement);

    push_test!(tests, harness_manager, cannot_become_own_parent);
    push_test!(tests, harness_manager, cannot_reparent_to_parent);
    push_test!(tests, harness_manager, cannot_reparent_to_stranger);
    push_test!(tests, harness_manager, nesting_child_becomes_parent);
    push_test!(tests, harness_manager, nesting_parent_becomes_child);
    push_test!(tests, harness_manager, cannot_create_looping_hierarchy);

    push_test!(tests, harness_manager, place_above_nested_parent);
    push_test!(tests, harness_manager, cannot_place_above_grandparent);
    push_test!(tests, harness_manager, cannot_place_above_parents_sibling);
    push_test!(tests, harness_manager, cannot_place_above_child);

    push_test!(tests, harness_manager, place_below_nested_parent);
    push_test!(tests, harness_manager, cannot_place_below_grandparent);
    push_test!(tests, harness_manager, cannot_place_below_parents_sibling);
    push_test!(tests, harness_manager, cannot_place_below_child);

    push_test!(tests, harness_manager, cannot_place_above_stranger);
    push_test!(tests, harness_manager, cannot_place_below_stranger);
    push_test!(tests, harness_manager, cannot_place_above_across_trees);
    push_test!(tests, harness_manager, cannot_place_below_across_trees);

    push_test!(tests, harness_manager, destruction);
}
