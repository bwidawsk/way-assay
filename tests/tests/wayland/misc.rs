use std::{
    os::fd::AsFd,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
};

use rustix::event::PollFlags;

use libtest_mimic::{Failed, Trial};

use way_assay::{
    harness::test_harness::{Harness, HarnessManager},
    test_common::{core::CoreState, wssy_poll, Client, EventHandler, EventlessState, WssyError},
};

use wayland_client::{
    protocol::{
        wl_compositor::WlCompositor,
        wl_display,
        wl_registry::{self, Event, WlRegistry},
    },
    Proxy,
};

use crate::{config::configure_timeout, expect_failure_code, push_test, to_client, wssy_assert};

use crate::tests::{bool_result, pass, LibtestReturn};

/// Tests that wl_compositor global is present, but do it... twice
fn wl_compositor_present<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + Sync,
{
    let count = Arc::new(AtomicUsize::new(0));
    let c = count.clone();
    let mut client = Client::new(harness);
    let reg_handler = EventHandler::new(move |_: WlRegistry, event| match event {
        Event::Global { interface, .. } => {
            if WlCompositor::interface().to_string() == interface
                && c.fetch_add(1, Ordering::Relaxed) == 1
            {}
        }
        _ => unreachable!(),
    });

    client.get_registry(reg_handler.clone());
    let _ = client.roundtrip();

    // and do it again to make sure it's still there :)
    client.get_registry(reg_handler);
    let _ = client.roundtrip();

    bool_result(
        count.load(Ordering::SeqCst) == 2,
        "wl_compositor wasn't found",
    )
}

fn create_region<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state_mut::<CoreState>();
    core.compositor_create_region(10, 10, 10, 10)?;
    pass()
}

fn sync_callback<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));

    let sync_future = client.sync();

    client.process_events_until(configure_timeout(), || sync_future.get().is_some())?;

    pass()
}

fn disconnect_on_error<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let registry = client.get_registry(Arc::new(EventlessState));

    // Bind a hopefully non-existant name
    let _bad_object = registry.send_constructor::<WlRegistry>(
        wl_registry::Request::Bind {
            name: 0xbeef,
            id: (WlRegistry::interface(), 1),
        },
        Arc::new(EventlessState),
    )?;

    expect_failure_code!(
        client.roundtrip(),
        wl_registry::WlRegistry,
        wl_display::Error::InvalidObject
    )?;

    // Check display hung up connection
    let fd = client.connection.as_fd();
    wssy_assert!(
        wssy_poll(fd, configure_timeout(), PollFlags::IN)?.contains(PollFlags::HUP),
        "Connection was not hung up"
    )
}

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    push_test!(tests, harness_manager, wl_compositor_present);
    push_test!(tests, harness_manager, create_region);
    push_test!(tests, harness_manager, sync_callback);
    push_test!(tests, harness_manager, disconnect_on_error);
}
