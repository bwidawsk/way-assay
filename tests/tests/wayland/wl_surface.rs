use std::sync::Arc;

use libtest_mimic::{Failed, Trial};

use way_assay::{
    harness::test_harness::{Harness, HarnessManager},
    test_common::{
        core::{BufferBuilder, CoreState},
        stable::xdg::{ToplevelBuilder, XdgStableState},
        Client, EventHandler, EventlessState, RequestVersion, WssyError, WssyFuture, WssyProtocol,
    },
};

use wayland_client::{
    protocol::{
        wl_callback::{self, WlCallback},
        wl_compositor::WlCompositor,
        wl_surface::{self, WlSurface},
    },
    Proxy, WEnum,
};
use wayland_protocols::xdg::shell::client::xdg_surface::{self, XdgSurface};

use crate::{
    config::configure_timeout, expect_failure_code, push_test, tests::pass, to_client, to_name,
};

use crate::tests::{roundtrip_result, LibtestReturn};

fn create<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    core.compositor_create_surface(Arc::new(EventlessState))?;

    roundtrip_result(&mut client)
}

fn attach_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let buffer = BufferBuilder::new(200, 200).build(&core)?;

    surface.attach(Some(&buffer), 0, 0);

    roundtrip_result(&mut client)
}

// wl_surface.attach:
// > When the bound wl_surface version is 5 or higher, passing any
// > non-zero x or y is a protocol violation, and will result in an
// > 'invalid_offset' error being raised.
fn v5_disallows_attach_offset<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let comp_v5: WssyProtocol = to_name!(WlCompositor).version(5);
    let mut client =
        to_client!(Client::new(harness).with_state::<CoreState>(Some(vec![comp_v5].into())));
    let core = client.state::<CoreState>();
    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let buffer = BufferBuilder::new(200, 200).build(&core)?;

    surface.attach(Some(&buffer), 4, 4);
    expect_failure_code!(
        client.roundtrip(),
        WlSurface,
        wl_surface::Error::InvalidOffset
    )
}

fn v4_allows_attach_offset<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let comp_v4: WssyProtocol = to_name!(WlCompositor).version(4);
    let mut client =
        to_client!(Client::new(harness).with_state::<CoreState>(Some(vec![comp_v4].into())));

    let core = client.state::<CoreState>();
    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let buffer = BufferBuilder::new(200, 200).build(&core)?;

    surface.attach(Some(&buffer), 4, 4);

    roundtrip_result(&mut client)
}

// wl_surface.set_buffer_scale:
// > If scale is not positive the invalid_scale protocol error is raised.
fn disallow_negative_size<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;

    surface.set_buffer_scale(-1);

    expect_failure_code!(
        client.roundtrip(),
        WlSurface,
        wl_surface::Error::InvalidScale
    )
}

// wl_surface.set_buffer_transform
// > If transform is not one of the values from the wl_output.transform enum the
// > invalid_transform protocol error is raised.
fn disallow_invalid_transform<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;

    surface.send_request(wl_surface::Request::SetBufferTransform {
        transform: WEnum::Unknown(0xbeef),
    })?;

    expect_failure_code!(
        client.roundtrip(),
        WlSurface,
        wl_surface::Error::InvalidTransform
    )
}

// wl_surface.attach:
// > The new size of the surface is calculated based on the buffer size
// > transformed by the inverse buffer_transform and the inverse buffer_scale.
// > This means that at commit time the supplied buffer size must be an integer
// > multiple of the buffer_scale. If that's not the case, an invalid_size error
// > is sent.
fn buffer_size_not_divisible_by_scale<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let buffer = BufferBuilder::new(9, 9).build(&core)?;

    surface.attach(Some(&buffer), 0, 0);
    surface.set_buffer_scale(2);
    surface.commit();

    expect_failure_code!(
        client.roundtrip(),
        WlSurface,
        wl_surface::Error::InvalidSize
    )
}

// wl_surface:
// > When a client wants to destroy a wl_surface, they must destroy this role
// > object before the wl_surface, otherwise a defunct_role_object error is
// > sent.
fn defunct_role_subsurface<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let parent_surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let child_surface = core.compositor_create_surface(Arc::new(EventlessState))?;

    let _sub_surface = core.subcompositor_get_subsurface(&child_surface, &parent_surface)?;

    child_surface.destroy();

    expect_failure_code!(
        client.roundtrip(),
        WlSurface,
        wl_surface::Error::DefunctRoleObject
    )
}

fn buffer_release_on_replace_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;

    let release_future = WssyFuture::new();
    let buffer_builder = BufferBuilder::new(200, 200);
    let buffers = [
        {
            let release_future = release_future.clone();
            buffer_builder
                .clone()
                .with_release_handler(move |_| release_future.set(()))
                .build(&core)?
        },
        buffer_builder.build(&core)?,
    ];

    surface.attach(Some(&buffers[0]), 0, 0);

    surface.commit();
    client.roundtrip()?;

    surface.attach(Some(&buffers[1]), 0, 0);

    surface.commit();
    client.roundtrip()?;

    client.process_events_until(configure_timeout(), || release_future.get().is_some())?;

    pass()
}

fn buffer_release_on_detach_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;

    let release_future = WssyFuture::new();
    let buffer = {
        let release_future = release_future.clone();
        BufferBuilder::new(200, 200)
            .with_release_handler(move |_| release_future.set(()))
            .build(&core)?
    };

    surface.attach(Some(&buffer), 0, 0);

    surface.commit();
    client.roundtrip()?;

    surface.attach(None, 0, 0);

    surface.commit();
    client.roundtrip()?;

    client.process_events_until(configure_timeout(), || release_future.get().is_some())?;

    pass()
}

fn frame_callback_first_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let core = client.state::<CoreState>();
    let xdg = client.state::<XdgStableState>();

    let configure_serial = WssyFuture::new();
    let frame_time = WssyFuture::new();

    let surface = core.compositor_create_surface(Arc::new(EventlessState))?;
    let xdg_surface = {
        let configure_serial = configure_serial.clone();
        xdg.get_xdg_surface(
            &surface,
            EventHandler::new(move |xdg_surface: XdgSurface, event| {
                if let xdg_surface::Event::Configure { serial } = event {
                    xdg_surface.ack_configure(serial);
                    configure_serial.set(serial);
                }
            }),
        )?
    };
    let _xdg_toplevel = xdg.get_xdg_toplevel(&xdg_surface, Arc::new(EventlessState))?;

    let _callback: WlCallback = {
        let frame_time = frame_time.clone();
        surface.send_constructor(
            wl_surface::Request::Frame {},
            EventHandler::new(move |_callback: WlCallback, event| {
                if let wl_callback::Event::Done { callback_data } = event {
                    frame_time.set(callback_data);
                }
            }),
        )?
    };

    surface.commit();

    client.process_events_until(configure_timeout(), || {
        frame_time.get().is_some() && configure_serial.get().is_some()
    })?;

    pass()
}

fn frame_callback_next_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = Client::new(harness.clone())
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;

    let (surface, _, _) =
        ToplevelBuilder::new().map(&harness, &mut client, configure_timeout(), |_, _, _| {
            BufferBuilder::new(10, 10)
        })?;

    client.roundtrip()?;

    let frame_time = WssyFuture::new();

    let _callback: WlCallback = {
        let frame_time = frame_time.clone();
        surface.send_constructor(
            wl_surface::Request::Frame {},
            EventHandler::new(move |_callback: WlCallback, event| {
                if let wl_callback::Event::Done { callback_data } = event {
                    frame_time.set(callback_data);
                }
            }),
        )?
    };

    surface.commit();

    client.process_events_until(configure_timeout(), || frame_time.get().is_some())?;

    pass()
}

pub fn add_tests(tests: &mut Vec<Trial>, harness_manager: impl HarnessManager) {
    push_test!(tests, harness_manager, create);
    push_test!(tests, harness_manager, attach_buffer);
    push_test!(tests, harness_manager, v5_disallows_attach_offset);
    push_test!(tests, harness_manager, v4_allows_attach_offset);
    push_test!(tests, harness_manager, disallow_negative_size);
    push_test!(tests, harness_manager, disallow_invalid_transform);
    push_test!(tests, harness_manager, buffer_size_not_divisible_by_scale);
    push_test!(tests, harness_manager, defunct_role_subsurface);
    push_test!(tests, harness_manager, buffer_release_on_replace_buffer);
    push_test!(tests, harness_manager, buffer_release_on_detach_buffer);
    push_test!(tests, harness_manager, frame_callback_first_buffer);
    push_test!(tests, harness_manager, frame_callback_next_buffer);
}
